﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A class for testing VoxelNavAgent
//Setup destinations for the agent to visit
//Run the agent automatically or step through the path manually

namespace VoxelMovement
{
    public class TestVoxelNavAgent : MonoBehaviour
    {
        public VoxelNavAgent navAgent;
        public Transform[] destinations;            //Setup destinations in the VoxelMap
        public KeyCode startKey = KeyCode.Return;   //Key for starting the agent if startOnAwake is disabled. If the agent autoUpdate is disabled this key steps through the path
        public bool startOnAwake = false;
        public bool loop = false;                   //Loop the destinations

        private int index = 0;

        // Start is called before the first frame update
        void Start()
        {
            //Hook up agent events
            if (navAgent != null)
            {
                navAgent.OnPathCompleted += OnPathCompleted;
                navAgent.OnPathInterrupted += OnPathInterrupted;
            }

            //Startup the agent
            if (startOnAwake && destinations.Length > 0)
            {
                if (navAgent.SetDestination(destinations[0].position) == false)
                    StartCoroutine(AttemptRePath());
            }
        }

        // Update is called once per frame
        void Update()
        {
            HandleInput();
        }

        private void HandleInput()
        {
            if (destinations.Length <= 0)
                return;

            if (Input.GetKeyDown(startKey))
            {
                //Start the agent or step through the path if agent's autoUpdate is disabled
                if (navAgent.Running == false)
                    navAgent.SetDestination(destinations[0].position);
                else if (navAgent.autoUpdate == false && navAgent.voxelBody.State == VoxelBody.MotionStates.STOPPED)
                    navAgent.UpdateVoxelBody();
            }
        }

        //Coroutine method for repathing if the agent path is obstructed
        IEnumerator AttemptRePath()
        {
            bool pathFound = false;

            while (pathFound == false)
            {
                yield return new WaitForSeconds(1f);
                pathFound = navAgent.SetDestination(destinations[index].position);
            }
        }

        //The agent successfully reached it's destination
        private void OnPathCompleted()
        {
            //Update destination index
            index++;

            if (index >= destinations.Length && loop)
                index = 0;

            if (index < destinations.Length)
            {
                //Get new destination from the array. If the destination is blocked start repathing routine
                if (navAgent.SetDestination(destinations[index].position) == false)
                    StartCoroutine(AttemptRePath());
            }
        }

        //The agent's destination was blocked
        private void OnPathInterrupted()
        {
            //Start repathing routine
            if (navAgent.autoReRoute)
                StartCoroutine(AttemptRePath());
        }
    }
}