﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ExampleVoxelBodyInput is used to control a VoxelBody

namespace VoxelMovement
{
    public class ExampleVoxelBodyInput : MonoBehaviour
    {
        public VoxelBody voxelBody;

        [Header("Motion curves")]
        public AnimationCurve movementCurve;
        public AnimationCurve rotationCurve;

        //Choose between keyboard keys and Unity's input axis
        [Space]
        public ControlTypes controlType = ControlTypes.KEYS;

        //Keyboard keys
        [Header("Input keys")]
        public KeyCode moveForwardKey = KeyCode.W;
        public KeyCode moveBackKey = KeyCode.S;
        public KeyCode moveRightKey = KeyCode.D;
        public KeyCode moveLeftKey = KeyCode.A;
        public KeyCode turnCWKey = KeyCode.E;
        public KeyCode turnCCWKey = KeyCode.Q;

        //Input axis allows for more versatile configuration in Project Settings -> Input
        [Header("Input axis")]
        public string verticalAxis = "";
        public string horizontalAxis = "";
        public string turnCWButton = "";
        public string turnCCWButton = "";

        public enum ControlTypes
        {
            AXIS,
            KEYS
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            UpdateControls();
        }

        public void UpdateControls()
        {
            if (voxelBody.State != VoxelBody.MotionStates.STOPPED)
                return;

            if (controlType == ControlTypes.AXIS)
            {
                if(string.IsNullOrEmpty(verticalAxis) == false && Input.GetAxis(verticalAxis) > 0.5f)
                    voxelBody.AddMovement(Point.forward, movementCurve);
                else if(string.IsNullOrEmpty(verticalAxis) == false && Input.GetAxis(verticalAxis) < -0.5f)
                    voxelBody.AddMovement(-Point.forward, movementCurve);

                if (string.IsNullOrEmpty(horizontalAxis) == false && Input.GetAxis(horizontalAxis) > 0.5f)
                    voxelBody.AddMovement(Point.right, movementCurve);
                else if (string.IsNullOrEmpty(horizontalAxis) == false && Input.GetAxis(horizontalAxis) < -0.5f)
                    voxelBody.AddMovement(-Point.right, movementCurve);

                if(string.IsNullOrEmpty(turnCWButton) == false && Input.GetButton(turnCWButton))
                    voxelBody.AddRotation(new Point(0, 1, 0), rotationCurve);
                if(string.IsNullOrEmpty(turnCWButton) == false && Input.GetButton(turnCCWButton))
                    voxelBody.AddRotation(new Point(0, -1, 0), rotationCurve);
            }
            else
            {
                if (Input.GetKey(moveForwardKey))
                    voxelBody.AddMovement(Point.forward, movementCurve);
                if (Input.GetKey(moveBackKey))
                    voxelBody.AddMovement(-Point.forward, movementCurve);
                if (Input.GetKey(moveLeftKey))
                    voxelBody.AddMovement(-Point.right, movementCurve);
                if (Input.GetKey(moveRightKey))
                    voxelBody.AddMovement(Point.right, movementCurve);

                if (Input.GetKey(turnCWKey))
                    voxelBody.AddRotation(new Point(0, 1, 0), rotationCurve);
                if (Input.GetKey(turnCCWKey))
                    voxelBody.AddRotation(new Point(0, -1, 0), rotationCurve);
            }
        }
    }
}