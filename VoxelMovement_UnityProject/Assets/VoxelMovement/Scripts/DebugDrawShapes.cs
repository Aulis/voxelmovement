﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//DebugDrawShapes is a static helper class for drawing shapes with Unity's debug methods

namespace VoxelMovement
{
    public static class DebugDrawShapes
    {
        public static void DrawRectangle(Vector3 position, float halfWidth, float halfHeight, Vector3 rotation, Color color, float duration)
        {
            Quaternion qRotation = Quaternion.Euler(rotation);

            Vector3 a = qRotation * new Vector3(-halfWidth, -halfHeight, 0f);
            Vector3 b = qRotation * new Vector3(-halfWidth, halfHeight, 0f);
            Vector3 c = qRotation * new Vector3(halfWidth, halfHeight, 0f);
            Vector3 d = qRotation * new Vector3(halfWidth, -halfHeight, 0f);

            Debug.DrawLine(position + a, position + b, color, duration);
            Debug.DrawLine(position + b, position + c, color, duration);
            Debug.DrawLine(position + c, position + d, color, duration);
            Debug.DrawLine(position + d, position + a, color, duration);
        }

        //Draw rectangle with default color and short duration
        public static void DrawRectangle(Vector3 position, float halfWidth, float halfHeight, Vector3 rotation)
        {
            DrawRectangle(position, halfWidth, halfHeight, rotation, Color.white, 1f);
        }
    }
}