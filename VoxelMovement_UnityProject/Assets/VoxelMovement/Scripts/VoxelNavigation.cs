﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Neighbours = VoxelMovement.Voxel.Directions;

//Static class for path calculation in the VoxelMap
//The path finding code is modified from Catlike Coding's Hex Map tutorial at https://catlikecoding.com/unity/tutorials/hex-map/part-16/

namespace VoxelMovement
{
    public static class VoxelNavigation
    {
        public static VoxelNavPath FindPath(Voxel start, Voxel end, LayerMask solidLayers)
        {
            if (start == null || end == null)
            {
                Debug.Log("VoxelNavigation: One of the voxels is null");
                return null;
            }

            if (start == end)
            {
                Debug.Log("VoxelNavigation: Path length zero");
                return null;
            }

            if (VoxelMap.InstanceNoSearch == null)
            {
                Debug.LogError("VoxelNavigation: Can't find VoxelMap");
                return null;
            }

            if (VoxelMap.InstanceNoSearch.MapBounds.IsPointInsideBounds(start.position) == false || VoxelMap.InstanceNoSearch.MapBounds.IsPointInsideBounds(end.position) == false)
            {
                Debug.Log("VoxelNavigation: One of the voxels is out of bounds");
                return null;
            }

            //Initialize search
            VoxelMap.InstanceNoSearch.ResetNavNodes();
            VoxelNavPath path = new VoxelNavPath();
            VoxelPriorityQueue searchFrontier = new VoxelPriorityQueue();   //searchFrontier keeps track of Voxels to examine
            Voxel current;

            start.navNode.distance = 0;
            searchFrontier.Enqueue(start);
            int distance = 0;

            //Run search as long as we have Voxels to examine
            while (searchFrontier.Count > 0)
            {
                //Get current examined node from frontier
                current = searchFrontier.Dequeue();

                //If we have reached the end then return the path
                if (current == end)
                {
                    //trace path back to start
                    List<Point> pathList = new List<Point>();
                    pathList.Add(end.position);

                    while (current != start && current != null)
                    {
                        current = current.navNode.pathFrom;
                        pathList.Add(current.position);
                    }
                    pathList.Reverse();
                    path.points = pathList.ToArray();

                    return path;
                }

                //Iterate current Voxel's neighbours
                for (int i = 0; i < 4; i++)
                {
                    Voxel neighbour = null;
                    switch (i)
                    {
                        case 0: neighbour = current.NeighbourForward; break;
                        case 1: neighbour = current.NeighbourRight; break;
                        case 2: neighbour = current.NeighbourBack; break;
                        case 3: neighbour = current.NeighbourLeft; break;
                        default: break;
                    }

                    //If there's no neighbour, or it's solid then we can't path here. Move on to next neighbour
                    if (neighbour == null)
                        continue;
                    if (neighbour.IsSolid(solidLayers))
                        continue;

                    //If the neighbour has solid bodies then we can't path here. Move on to next neighbour
                    if (neighbour.Bodies.Count > 0)
                    {
                        bool isSolid = false;
                        for (int j = 0; j < neighbour.Bodies.Count; j++)
                        {
                            //If the neighbour has an active solid body
                            if (neighbour.Bodies[j].isActiveAndEnabled == true && neighbour.Bodies[j].isBeingRemoved == false && neighbour.Bodies[j].IsSolid(solidLayers))
                            {
                                //If the body has an agent but it's going to move somewhere we can ignore it
                                if (neighbour.Bodies[j].agent != null && neighbour.Bodies[j].agent.Destination != neighbour && TravelDistanceTo(start, neighbour) > 1)
                                    continue;
                                isSolid = true;
                                break;
                            }
                        }
                        if (isSolid)
                            continue;
                    }

                    distance = current.navNode.distance + 1;

                    //If this neighbour's distance is int.max it means we haven't encountered this neighbour yet
                    if (neighbour.navNode.distance == int.MaxValue)
                    {
                        neighbour.navNode.distance = distance + neighbour.navNode.navCost;
                        neighbour.navNode.pathFrom = current;

                        //Estimate shortest distance to the end and add the neighbour to the frontier
                        neighbour.navNode.searchHeuristic = TravelDistanceTo(neighbour, end);
                        searchFrontier.Enqueue(neighbour);
                    }
                    //if we did visit this neighbour before
                    else
                    {
                        //If we came here from a shorter route then configure neighbour's path from current Voxel
                        distance += neighbour.navNode.navCost;
                        if (distance < neighbour.navNode.distance)
                        {
                            int oldPriority = neighbour.navNode.SearchPriority;
                            neighbour.navNode.distance = distance;
                            neighbour.navNode.pathFrom = current;
                            searchFrontier.Change(neighbour, oldPriority);
                        }
                    }
                }
            }

            //If the search didn't reach the end Voxel there's no path
            return null;
        }

        //Shortest travel distance between two voxels
        public static int TravelDistanceTo(Voxel start, Voxel end)
        {
            return (Mathf.Abs(end.position.x - start.position.x) +
                    Mathf.Abs(end.position.y - start.position.y) +
                    Mathf.Abs(end.position.z - start.position.z)) / 2;
        }

        //Draw debug rectangle on given Voxel
        public static void PaintVoxel(Point mapPosition, Color color, float duration)
        {
            Vector3 position = VoxelMap.InstanceNoSearch.GetWorldPosition(mapPosition);

            DebugDrawShapes.DrawRectangle(position, VoxelMap.Instance.unitSize.x * 0.25f, VoxelMap.Instance.unitSize.z * 0.25f, new Vector3(90f, 0f, 0f), color, duration);
        }

        //Navigation data for Voxels
        [System.Serializable]
        public class VoxelNavNode
        {
            public int navCost = 0;                     //Setting navCost will make agents avoid the Voxel
            public int distance = int.MaxValue;         //Distance from the start of the path
            public int searchHeuristic = 0;             //Shortest distance to path end
            public Voxel pathFrom = null;               //The Voxel we arrived here from. Set during path finding

            public Voxel nextWithSamePriority = null;   //Linked list to other Voxels with the same priority

            public int SearchPriority { get { return distance + searchHeuristic; } }    //Search priority is based on the calculated path length so far and the shortest estimated distance to path end.
                                                                                        //Nodes with the smallest priority are examined first.
            public void Reset()
            {
                distance = int.MaxValue;
                searchHeuristic = 0;
                pathFrom = null;
            }
        }

        //Custom queue class for path finding
        public class VoxelPriorityQueue
        {
            List<Voxel> list = new List<Voxel>();
            int count = 0;
            int minimum = int.MaxValue;     //Index of first non-null Voxel in the list. This is the same as the VoxelNavNode's search priority

            public int Count
            {
                get { return count; }
            }

            public void Enqueue(Voxel voxel)
            {
                count++;

                //Voxels are arranged in the queue based on their search priority. Any gaps are filled with null items
                int priority = voxel.navNode.SearchPriority;
                if (priority < minimum)
                    minimum = priority;
                while (priority >= list.Count)
                    list.Add(null);

                //Any existing Voxel in this index is moved to a linked list
                voxel.navNode.nextWithSamePriority = list[priority];
                list[priority] = voxel;
            }

            public Voxel Dequeue()
            {
                count--;

                //Get the first non-null item (smallest search priority)
                for (; minimum < list.Count; minimum++)
                {
                    Voxel voxel = list[minimum];
                    if (voxel != null)
                    {
                        //Set the next item in the linked list into the queue spot. Queue spot will turn null if the list is empty
                        list[minimum] = voxel.navNode.nextWithSamePriority;
                        return voxel;
                    }
                }
                return null;
            }

            //Change a Voxel's search priority
            public void Change(Voxel voxel, int oldPriority)
            {
                Voxel current = list[oldPriority];
                Voxel next = current.navNode.nextWithSamePriority;

                //If the Voxel is the first item in the linked list then move the next one first
                if (current == voxel)
                    list[oldPriority] = next;
                //Else find the Voxel and join adjacent ones in the linked list
                else
                {
                    while (next != voxel)
                    {
                        current = next;
                        next = current.navNode.nextWithSamePriority;
                    }
                    current.navNode.nextWithSamePriority = voxel.navNode.nextWithSamePriority;
                }

                //Re-add the Voxel with the new search priority
                Enqueue(voxel);
                count--;
            }

            public void Clear()
            {
                list.Clear();
                count = 0;
                minimum = int.MaxValue;
            }
        }

        public class VoxelNavPath
        {
            public Point[] points;
        }
    }
}