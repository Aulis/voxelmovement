﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//VoxelNavProperties component allows the user to set navigation related properties for Voxels in the editor
//Add this component to a level element under VoxelMap gameobject.

namespace VoxelMovement
{
    public class VoxelNavProperties : MonoBehaviour
    {
        public int navCost = 0;     //The cost of traversing this Voxel, considered when calculating paths
    }
}