﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.Events;

//The VoxelBody is a script for objects that need to move square by square along the VoxelMap
//A VoxelBody can move in any six directions or rotate along the three axis
//VoxelBodies move by immediately setting their new positions, then transitioning there from the old position
//Movement and rotation is done with animation curves
//VoxelBodyManager handles updating the VoxelBodies and collisions
//-Use AddMovement & AddRotation methods to request for motion
//-Use the Debug feature to visualize movement and bounds

namespace VoxelMovement
{
    public class VoxelBody : MonoBehaviour
    {
        public Rigidbody rigidBody;                 //Optional RigidBody if you need collisions with PhysX objects. Set to kinematic.
        [HideInInspector]
        public VoxelNavAgent agent;

        //Movement related values
        private Point position;                     //The VoxelBody's position on the map
        private Point previousPosition;
        private Voxel voxel;                        //The Voxel the body is currently occupying
        private Point movementDirection;
        [Header("Movement")]
        public AnimationCurve movementCurve = new AnimationCurve();     //Default animation curve for movement. Y-axis is transition (0-1), X-axis is time
        public bool continuousMovement = false;                         //Enable to keep the body moving until it hits something
        public bool continuousInput = true;                             //Enable to allow direction change mid movement
        public float pDirChangeStartMargin = 0.2f;  //transition start margin for input with perpendicular direction change
        public float pDirChangeEndMargin = 0.95f;   //transition end margin for input with perpendicular direction change
        private bool hasMoved = false;

        //Rotation related values
        private Point rotation;
        private Point previousRotation;
        [Header("Rotation")]
        public AnimationCurve rotationCurve = new AnimationCurve();     //Default animation curve for rotation. Y-axis is transition (0-1), X-axis is time

        //Shared values for movement and rotation
        private MotionStates motionState = MotionStates.STOPPED;        //VoxelBody's current motion state. Can be STOPPED, MOVING or ROTATING
        private MotionData motionToApply;                               //Request for next motion. VoxelBodyManager applies these
        private float motionTransitionTime = 0f;
        private float motionTransition = 0f;

        //Collision related values
        [Header("Collision")]
        public LayerMask solidLayers;               //GameObject Layers that the VoxelBody can collide with
        public bool triggerType = false;            //As a trigger the VoxelBody gets collision events, but it's movement or position is not affected by them
        public bool pushable = false;               //Enable to allow this VoxelBody to be pushed
        public bool canPush = false;                //Enable to allow this VoxelBody to push others
        public Bounds fineBounds;                   //Allow the VoxelBody to be detected on the Voxels it overlaps during movement
        [HideInInspector]
        public List<VoxelBody> collidingBodies = new List<VoxelBody>(); //Temporary reference to all colliding bodies during this frame

        //Grid transform point vectors
        private Point up = Point.up;
        private Point forward = Point.forward;
        private Point right = Point.right;

        //Debug values
        [Header("Debug")]
        public bool debug = false;
        public Color debugPosColor = Color.white;
        public Color debugPreviousPosColor = Color.red;
        public Color debugFineSizeColor = Color.yellow;

        //Events
        //UnityEvents for hooking up in the editor
        [Header("Events")]
        public UnityEventPoint OnMovingUE;              //Point is movement direction
        public UnityEventPoint OnStoppedUE;             //Point is position
        public UnityEventPoint OnFinishedMovingUE;      //Point is position
        public UnityEventPoint OnFailedMovingUE;        //Point is position
        public UnityEventPoint OnRotatingUE;            //Point is delta rotation
        public UnityEventPoint OnStoppedRotationUE;     //Point is rotation

        public System.Action OnInitialized;
        public System.Action<List<VoxelBody>> OnEnterBodyCollision;
        public System.Action<Voxel> OnEnterVoxelCollision;
        public System.Action<Point> OnMoving;           //Point is movement direction
        public System.Action<Point> OnStopped;          //Point is position
        public System.Action<Point> OnFinishedMoving;   //Point is position
        public System.Action<Point> OnFailedMoving;     //Point is position
        public System.Action<Point> OnDirectionChanged; //Point is movement direction
        public System.Action<Point> OnRotating;         //Point is delta rotation
        public System.Action<Point> OnStoppedRotation;  //Point is rotation

        //Manager values
        [HideInInspector]
        public bool isBeingRemoved = false;

        public enum MotionStates
        {
            STOPPED,
            MOVING,
            ROTATING
        };

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public Point Up { get { return up; } }
        public Point Forward { get { return forward; } }
        public Point Right { get { return right; } }

        //Setting the position also moves the Transform to the correct map location
        public Point Position
        {
            get { return position; }
            set
            {
                Vector3 posDelta = (value - position) * VoxelMap.Instance.unitSize;
                Point prevPosDelta = previousPosition - position;

                position = value;
                previousPosition = position + prevPosDelta;

                UpdateVoxel();

                if (motionState == MotionStates.MOVING)
                    transform.position += posDelta;
                else
                    transform.position = VoxelMap.Instance.GetWorldPosition(position);
            }
        }

        public Point PreviousPosition { get { return previousPosition; } }

        public Voxel CurrentVoxel { get { return voxel; } }

        //Setting the rotation also rotates the Transform
        public Point Rotation
        {
            get { return rotation; }
            set
            {
                Vector3 rotDelta = (value - rotation) * 90f;
                Point prevRotDelta = previousRotation - rotation;

                rotation = value;
                previousRotation = rotation + prevRotDelta;

                up = Point.Rotated90(Point.up, rotation);
                right = Point.Rotated90(Point.right, rotation);
                forward = Point.Rotated90(Point.forward, rotation);

                if (motionState == MotionStates.ROTATING)
                    transform.rotation *= Quaternion.Euler(rotDelta);
                else
                    transform.rotation = Quaternion.Euler(rotation * 90f);
            }
        }

        public Point PreviousRotation { get { return previousRotation; } }

        public Point MovementDirection { get { return movementDirection; } }

        public MotionStates State { get { return motionState; } }

        public float MotionTransition { get { return motionTransition; } }

        //-----------------------------------------------
        //MONOBEHAVIOUR
        //-----------------------------------------------

        private void OnValidate()
        {
            Vector3 extents = fineBounds.extents;

            if (extents.x < 0f)
                extents.x = 0f;
            if (extents.y < 0f)
                extents.y = 0f;
            if (extents.z < 0f)
                extents.z = 0f;

            fineBounds.extents = extents;

            pDirChangeStartMargin = Mathf.Clamp(pDirChangeStartMargin, 0f, 1f);
            pDirChangeEndMargin = Mathf.Clamp(pDirChangeEndMargin, 0f, 1f);
        }

        private void Awake()
        {
            //Assign self to VoxelBodymanager if possible
            VoxelBodyManager.Instance.AddVoxelBody(this);

            InitializePositionAndRotation();
        }

        private void OnDestroy()
        {
            //Unregister from VoxelBodymanager and exit the voxel if possible
            if (VoxelBodyManager.InstanceNoCreate != null)
                VoxelBodyManager.InstanceNoCreate.RemoveVoxelBody(this);

            if (voxel != null)
                voxel.ExitVoxel(this);
        }

        //-------------------------------------------------
        //INITIALIZATION METHODS
        //-------------------------------------------------

        public void InitializePositionAndRotation()
        {
            //VoxelMap required to initialize
            if (VoxelMap.Instance == null)
                return;

            //Read body position from transform, and set to correct increments
            Position = VoxelMap.Instance.GetMapPosition(transform.position);
            previousPosition = position;

            //Read rotation from transform, and set to correct increments
            Rotation = new Point(Mathf.FloorToInt(transform.eulerAngles.x / 90f),
                Mathf.FloorToInt(transform.eulerAngles.y / 90f),
                Mathf.FloorToInt(transform.eulerAngles.z / 90f));
            previousRotation = rotation;

            //Initialize motion related values
            motionState = MotionStates.STOPPED;
            motionTransitionTime = 0f;
            motionTransition = 1f;
            motionToApply = null;

            if (OnInitialized != null)
                OnInitialized();
        }

        //-------------------------------------------------
        //CONTROL METHODS
        //-------------------------------------------------

        //Use this method to request for movement
        public void AddMovement(Point direction, AnimationCurve motionCurve = null, float transitionOffset = 0f)
        {
            //If we don't have continuous input enabled and haven't stopped then ignore
            if (continuousInput == false && motionState != MotionStates.STOPPED)
                return;

            //Set motion to apply by the VoxelBodyManager
            if (this.isActiveAndEnabled)
                motionToApply = new MotionData(direction, motionCurve, MotionData.MotionType.MOVEMENT, transitionOffset);
        }

        //Use this method to request for rotation
        public void AddRotation(Point direction, AnimationCurve motionCurve = null, float transitionOffset = 0f)
        {
            //If we don't have continuous input enabled and haven't stopped then ignore
            if (continuousInput == false && motionState != MotionStates.STOPPED)
                return;

            //Set motion to apply by the VoxelBodyManager
            if (this.isActiveAndEnabled)
                motionToApply = new MotionData(direction, motionCurve, MotionData.MotionType.ROTATION, transitionOffset);
        }

        //Stop the VoxelBody motion on the final position and rotation
        public void SetStopped(bool clearMotionData = true)
        {
            //Set the correct increment to transform
            transform.position = VoxelMap.Instance.GetWorldPosition(position);
            transform.rotation = Quaternion.Euler(rotation * 90f);

            //Rotate directional vectors
            up = Point.Rotated90(Point.up, rotation);
            forward = Point.Rotated90(Point.forward, rotation);
            right = Point.Rotated90(Point.right, rotation);

            previousPosition = position;
            previousRotation = rotation;

            //Complete transition values
            motionTransitionTime = 0f;
            motionTransition = 1f;

            if (clearMotionData)
                motionToApply = null;

            //Trigger events depending on previous state
            if (motionState == MotionStates.MOVING)
            {
                motionState = MotionStates.STOPPED;
                if (hasMoved)
                {
                    hasMoved = false;
                    if (OnStopped != null)
                        OnStopped(position);
                    OnStoppedUE.Invoke(position);
                }
            }
            else if (motionState == MotionStates.ROTATING)
            {
                motionState = MotionStates.STOPPED;
                if (OnStoppedRotation != null)
                    OnStoppedRotation(rotation);
                OnStoppedRotationUE.Invoke(rotation);
            }
        }

        //Manually set the VoxelBody state and motion values. Used by VoxelBodyManager when pushing or by AI.
        public void SetState(Point newPosition, Point newPreviousPosition, Point newRotation, Point newPreviousRotation, float newMotionTransitionTime, Point newMovementdirection, MotionStates newMotionstate, AnimationCurve newMovementCurve = null, AnimationCurve newRotationCurve = null)
        {
            //Check for changes
            bool directionChanged = movementDirection != newMovementdirection;
            bool movementStateChanged = motionState != newMotionstate;

            //Set position, rotation and curves
            position = newPosition;
            UpdateVoxel();
            previousPosition = newPreviousPosition;
            rotation = newRotation;
            previousRotation = newPreviousRotation;
            if (newMovementCurve != null)
                movementCurve = newMovementCurve;
            if (newRotationCurve != null)
                rotationCurve = newRotationCurve;

            //Rotate directional vectors
            up = Point.Rotated90(Point.up, newRotation);
            forward = Point.Rotated90(Point.forward, newRotation);
            right = Point.Rotated90(Point.right, newRotation);

            //Set transition time and progress based on state
            motionTransitionTime = newMotionTransitionTime;
            switch (newMotionstate)
            {
                case MotionStates.STOPPED: motionTransition = 1f; break;
                case MotionStates.MOVING: motionTransition = newMovementCurve.Evaluate(motionTransitionTime); break;
                case MotionStates.ROTATING: motionTransition = newRotationCurve.Evaluate(motionTransitionTime); break;
                default: motionTransition = 1f; break;
            }

            movementDirection = newMovementdirection;
            motionState = newMotionstate;

            //Solve transform position
            Vector3 startPos = VoxelMap.Instance.GetWorldPosition(newPreviousPosition);
            Vector3 endPos = VoxelMap.Instance.GetWorldPosition(newPosition);
            if (rigidBody == null)
                transform.position = Vector3.Lerp(startPos, endPos, motionTransition);
            else
                rigidBody.MovePosition(Vector3.Lerp(startPos, endPos, motionTransition));

            //Solve transform rotation
            Vector3 startRot = previousRotation * 90f;
            Vector3 endRot = rotation * 90f;
            if (rigidBody == null)
                transform.rotation = Quaternion.Euler(Vector3.Lerp(startRot, endRot, motionTransition));
            else
                rigidBody.MoveRotation(Quaternion.Euler(Vector3.Lerp(startRot, endRot, motionTransition)));

            //Trigger events if movement state or direction was changed
            if (movementStateChanged)
            {
                switch (motionState)
                {
                    case MotionStates.STOPPED:
                        if (OnStopped != null)
                            OnStopped(newPosition);
                        OnStoppedUE.Invoke(newPosition);
                        break;

                    case MotionStates.MOVING:
                        if (OnMoving != null)
                            OnMoving(movementDirection);
                        OnMovingUE.Invoke(movementDirection);
                        break;

                    case MotionStates.ROTATING:
                        if (OnRotating != null)
                            OnRotating(rotation - previousRotation);
                        OnRotatingUE.Invoke(rotation - previousRotation);
                        break;

                    default: break;
                }
            }
            if (directionChanged && OnDirectionChanged != null)
                OnDirectionChanged(newMovementdirection);
        }

        //-------------------------------------------------
        //MOTION UPDATE METHODS. USED BY VOXELBODYMANAGER
        //-------------------------------------------------

        //Update voxel body motion based on motion state
        public void UpdateMotion()
        {
            if (isActiveAndEnabled == false || isBeingRemoved)
                return;

            switch (motionState)
            {
                case MotionStates.STOPPED:
                    UpdateStoppedState(); break;
                case MotionStates.MOVING:
                    UpdateMovingState(); break;
                case MotionStates.ROTATING:
                    UpdateRotatingState(); break;
                default: break;
            }
        }

        //Initiate motion if there is motion data to apply
        private void UpdateStoppedState()
        {
            //If there's a request for motion
            if (motionToApply != null)
            {
                //If the request is for movement
                if (motionToApply.type == MotionData.MotionType.MOVEMENT)
                {
                    //Apply movement request
                    bool directionChanged = movementDirection != motionToApply.direction;
                    movementDirection = motionToApply.direction;
                    if (motionToApply.curve != null)
                        movementCurve = motionToApply.curve;

                    //Other preparations for move
                    motionState = VoxelBody.MotionStates.MOVING;
                    previousPosition = position;
                    position += movementDirection;
                    UpdateVoxel();
                    motionTransitionTime = motionToApply.transitionTimeOffset;
                    motionTransition = 0f;

                    motionToApply = null;

                    //Trigger events
                    if (directionChanged && OnDirectionChanged != null)
                        OnDirectionChanged(movementDirection);
                }
                //If the request is for rotation
                else
                {
                    //Apply rotation request
                    if (motionToApply.curve != null)
                        rotationCurve = motionToApply.curve;

                    //Other preparations for rotation
                    motionState = VoxelBody.MotionStates.ROTATING;
                    previousRotation = rotation;
                    rotation += motionToApply.direction;
                    motionTransitionTime = motionToApply.transitionTimeOffset;
                    motionTransition = 0f;

                    //Rotate directional vectors
                    Point delta = rotation - previousRotation;
                    up.Rotate90(delta);
                    forward.Rotate90(delta);
                    right.Rotate90(delta);

                    motionToApply = null;

                    //Trigger events
                    if (OnRotating != null)
                        OnRotating(rotation - previousRotation);
                    OnRotatingUE.Invoke(rotation - previousRotation);
                }
            }
        }

        //Handle voxel body movement
        private void UpdateMovingState()
        {
            //Trigger events if began moving
            if (hasMoved == false)
            {
                hasMoved = true;
                if (OnMoving != null)
                    OnMoving(movementDirection);
                OnMovingUE.Invoke(movementDirection);
            }

            float motionDuration = movementCurve[movementCurve.length - 1].time;

            //Solve transition values
            motionTransitionTime += Time.deltaTime;
            motionTransition = movementCurve.Evaluate(motionTransitionTime);

            //If there was a new motion request during movement, change direction
            if (motionToApply != null && motionToApply.type == MotionData.MotionType.MOVEMENT)
            {
                //If the request was in a different direction than current movement
                if (motionToApply.direction != movementDirection)
                {
                    //Opposite direction
                    if ((motionToApply.direction.z > 0 && movementDirection.z < 0) || (motionToApply.direction.z < 0 && movementDirection.z > 0) ||
                        (motionToApply.direction.x < 0 && movementDirection.x > 0) || (motionToApply.direction.x > 0 && movementDirection.x < 0))
                    {
                        if (motionToApply.transitionTimeOffset <= 0f)
                            motionTransitionTime = motionDuration - motionTransitionTime;
                        else
                            motionTransitionTime = motionToApply.transitionTimeOffset;

                        //Apply request
                        movementDirection = motionToApply.direction;
                        if (motionToApply.curve != null)
                            movementCurve = motionToApply.curve;
                        motionToApply = null;

                        Point tempPosition = position;
                        position = previousPosition;
                        UpdateVoxel();
                        previousPosition = tempPosition;

                        motionDuration = movementCurve[movementCurve.length - 1].time;
                        motionTransition = movementCurve.Evaluate(motionTransitionTime);

                        if (OnDirectionChanged != null)
                            OnDirectionChanged(movementDirection);
                    }
                    //Perpendicular direction with some margin
                    else if (motionTransition < pDirChangeStartMargin || motionTransition >= pDirChangeEndMargin)
                    {
                        //Apply request
                        movementDirection = motionToApply.direction;
                        if (motionToApply.curve != null)
                            movementCurve = motionToApply.curve;
                        motionTransitionTime = motionToApply.transitionTimeOffset;
                        motionToApply = null;

                        previousPosition = VoxelMap.Instance.GetMapPosition(transform.position);
                        position = previousPosition + movementDirection;
                        UpdateVoxel();

                        motionDuration = movementCurve[movementCurve.length - 1].time;
                        motionTransition = movementCurve.Evaluate(motionTransitionTime);

                        if (OnDirectionChanged != null)
                            OnDirectionChanged(movementDirection);
                    }
                }
            }

            //Update position
            Vector3 startPos = VoxelMap.Instance.GetWorldPosition(previousPosition);
            Vector3 endPos = VoxelMap.Instance.GetWorldPosition(position);
            if (rigidBody == null)
                transform.position = Vector3.Lerp(startPos, endPos, motionTransition);
            else
                rigidBody.MovePosition(Vector3.Lerp(startPos, endPos, motionTransition));

            //End of transition reached
            if (motionTransitionTime >= motionDuration)
            {
                previousPosition = position;

                //If we still wanna move, get control direction and continue
                if (continuousMovement)
                {
                    motionTransitionTime -= motionDuration;
                    motionTransition = movementCurve.Evaluate(motionTransitionTime);
                    position += movementDirection;
                    UpdateVoxel();
                }
                //Else stop
                else
                {
                    SetStopped(false);
                    if (OnFinishedMoving != null)
                        OnFinishedMoving(position);
                    OnFinishedMovingUE.Invoke(position);
                }
            }
        }

        //Handle voxel body rotation
        private void UpdateRotatingState()
        {
            float motionDuration = rotationCurve[rotationCurve.length - 1].time;

            motionTransitionTime += Time.deltaTime;
            motionTransition = rotationCurve.Evaluate(motionTransitionTime);

            //Update rotation
            Vector3 startRot = previousRotation * 90f;
            Vector3 endRot = rotation * 90f;

            if (rigidBody == null)
                transform.rotation = Quaternion.Euler(Vector3.Lerp(startRot, endRot, motionTransition));
            else
                rigidBody.MoveRotation(Quaternion.Euler(Vector3.Lerp(startRot, endRot, motionTransition)));

            //End of transition reached
            if (motionTransitionTime >= motionDuration)
                SetStopped(false);
        }

        //-------------------------------------------------
        //UTILITY METHODS
        //-------------------------------------------------

        //UpdateVoxel sets the Voxel the body is occupying, if position has changed
        private void UpdateVoxel()
        {
            //We only need to exit voxels if we have them and our position has changed
            if (voxel != null && position != voxel.position)
            {
                voxel.ExitVoxel(this);
                voxel = null;
            }

            //Only enter voxels if we we don't have a voxel or our position has changed
            if (voxel != null && position == voxel.position)
                return;
            voxel = VoxelMap.Instance.GetVoxel(position);
            voxel.EnterVoxel(this);
        }

        //Check if this voxel body is solid
        public bool IsSolid(LayerMask solidLayers)
        {
            return ((1 << gameObject.layer) & solidLayers.value) > 0;
        }

        //Turn a Point direction in world space into body's local direction
        public Point RelativeDirection(Point worldDirection)
        {
            Point relativeDir = new Point();
            relativeDir.z = Point.DotProduct(forward, worldDirection);
            relativeDir.x = Point.DotProduct(right, worldDirection);
            relativeDir.y = Point.DotProduct(up, worldDirection);
            return relativeDir;
        }

        //----------------------------------------------
        //DEBUG METHODS
        //----------------------------------------------

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (debug && VoxelBodyManager.InstanceNoCreate != null && EditorApplication.isPlaying)
            {
                //Draw previous position wirecube
                Gizmos.color = debugPreviousPosColor;
                Gizmos.DrawWireCube(VoxelMap.InstanceNoSearch.GetWorldPosition(previousPosition), VoxelMap.InstanceNoSearch.unitSize);

                //Draw current position wirecube
                Gizmos.color = debugPosColor;
                Gizmos.DrawWireCube(VoxelMap.InstanceNoSearch.GetWorldPosition(position), VoxelMap.InstanceNoSearch.unitSize);

                //Draw fine bounds wirecube
                Gizmos.color = debugFineSizeColor;
                Gizmos.DrawWireCube(transform.position + fineBounds.center, fineBounds.extents * 2f);
            }
        }
#endif

        //----------------------------------------------
        //NESTED CLASSES
        //----------------------------------------------

        //Motion request data class
        public class MotionData
        {
            public Point direction;
            public AnimationCurve curve;
            public MotionType type;
            public float transitionTimeOffset = 0f;

            public enum MotionType
            {
                MOVEMENT,
                ROTATION
            }

            public MotionData(Point direction, AnimationCurve curve, MotionType type, float transitionOffset)
            {
                this.direction = direction;
                this.curve = curve;
                this.type = type;
                if (curve != null)
                    this.transitionTimeOffset = curve[curve.length - 1].time * transitionOffset;
                else
                    this.transitionTimeOffset = 0f;
            }
        }
    }
}