﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//VoxelMap is a simple map system for games using grid based movement
//Place this component in the root object of your map and create the map from objects placed as children
//When running, the map is built from the children and aligned according to the unitSize values
//VoxelGroups can be used to organize the children into groups and make the hierarchy easier to read
//It's advised to decide on the unitSize of the map first, then build the level from elements of that size
//The map can be traversed square by square by VoxelBodies with the help of VoxelBodyManager
//The VoxelMap is a Monobehaviour singleton. Only one instance is needed per scene.

namespace VoxelMovement
{
    public class VoxelMap : MonoBehaviour
    {

        private static VoxelMap instance;

        [Tooltip("Set this to the unit size of your level elements")]
        public Vector3 unitSize = Vector3.one;

        public bool debug = false;
        public Color debugOriginColor = Color.white;
        public Color debugBoundsColor = Color.white;

        private Voxel[,,] map;
        private PointBounds mapBounds;
        private Vector3 mapLocalOffset = Vector3.zero;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public static VoxelMap Instance
        {
            get
            {
                if (instance == null)
                {
                    //If instance doesn't exist look for one in the scene
                    instance = FindObjectOfType(typeof(VoxelMap)) as VoxelMap;
                    if (instance != null)
                        instance.BuildMapFromChildren();
                }

                return instance;
            }
        }

        public static VoxelMap InstanceNoSearch { get { return instance; } }

        public Voxel[,,] Map { get { return map; } }

        public PointBounds MapBounds { get { return mapBounds; } }

        //-----------------------------------------------
        //MONOBEHAVIOUR
        //-----------------------------------------------

        private void OnValidate()
        {
            if (unitSize.x < 0.1f)
                unitSize.x = 0.1f;
            if (unitSize.y < 0.1f)
                unitSize.y = 0.1f;
            if (unitSize.z < 0.1f)
                unitSize.z = 0.1f;
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                BuildMapFromChildren();
            }
        }

        //-------------------------------------------------
        //MAP GENERATION METHODS
        //-------------------------------------------------

        private void BuildMapFromChildren()
        {
            List<Vector3> childPositions = new List<Vector3>();

            //Figure out map bounds from children positions
            for (int i = 0; i < transform.childCount; i++)
            {
                //Get child
                GameObject go = transform.GetChild(i).gameObject;

                //If the child has a VoxelGroup component then get the children from its groupTransform
                VoxelGroup group = go.GetComponent<VoxelGroup>();
                if (group != null)
                {
                    if (group.groupTransform != null)
                    {
                        for (int j = 0; j < group.groupTransform.childCount; j++)
                            childPositions.Add(group.groupTransform.GetChild(j).position);
                    }
                }
                else
                    childPositions.Add(go.transform.position);
            }

            //Create map array with bounds & offset
            mapBounds = SolveMapBounds(childPositions);
            map = new Voxel[mapBounds.size.x, mapBounds.size.y, mapBounds.size.z];
            mapLocalOffset.x = mapBounds.min.x * unitSize.x;
            mapLocalOffset.y = mapBounds.min.y * unitSize.y;
            mapLocalOffset.z = mapBounds.min.z * unitSize.z;

            if (debug)
            {
                Debug.Log("Map size: " + mapBounds.size);
                Debug.Log("Map offset: " + mapLocalOffset);
            }

            //Place children into voxels & voxels into map array
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                //Get the child
                GameObject go = transform.GetChild(i).gameObject;

                //If the child has a VoxelGroup component then get the children from it's groupTransform
                VoxelGroup group = go.GetComponent<VoxelGroup>();
                if (group != null)
                {
                    if (group.groupTransform != null)
                    {
                        for (int j = group.groupTransform.childCount - 1; j >= 0; j--)
                            AddVoxel("Voxel", group.groupTransform.GetChild(j).gameObject, group.groupTransform);
                    }
                }
                else
                    AddVoxel("Voxel", go, transform);
            }

            int zLength = map.GetLength(2);
            int yLength = map.GetLength(1);
            int xLength = map.GetLength(0);

            //Insert empty voxels into map
            for (int z = 0; z < zLength; z++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    for (int x = 0; x < xLength; x++)
                    {
                        if (map[x, y, z] != null)
                            continue;

                        AddVoxel("Empty", null, transform, new Point(x, y, z));
                    }
                }
            }
            GenerateNeigbours();
        }

        //Create voxel with map position
        private bool AddVoxel(string voxelName, GameObject go, Transform parent, Point position)
        {
            //Check that map exists and that position is not out of bounds
            if (map == null)
                return false;

            if (position.x < 0 || position.x >= map.GetLength(0) || position.y < 0 || position.y >= map.GetLength(1) || position.z < 0 || position.z >= map.GetLength(2))
                return false;

            //Create voxel object and component
            GameObject voxObject = new GameObject();
            Voxel vox = voxObject.AddComponent<Voxel>();

            //Set voxel position & name
            vox.position = position;
            voxObject.name = voxelName + ": " + vox.position.x + ", " + vox.position.y + ", " + vox.position.z;
            voxObject.transform.position = GetWorldPosition(vox.position);
            voxObject.transform.SetParent(parent);

            //Set child related data
            vox.child = go;

            if (go != null)
            {
                //If child has nav properties, set them to voxel
                if (go.GetComponent<VoxelNavProperties>() != null)
                    vox.navNode.navCost = go.GetComponent<VoxelNavProperties>().navCost;

                //If child has a voxel body, initialize it
                VoxelBody body = go.GetComponent<VoxelBody>();
                if (body != null)
                {
                    VoxelBodyManager.Instance.AddVoxelBody(body);
                    body.Position = vox.position;
                }

                //Set child into voxel
                go.transform.SetParent(voxObject.transform);
                go.transform.localPosition = Vector3.zero;
            }

            map[vox.position.x, vox.position.y, vox.position.z] = vox;

            return true;
        }

        //Create voxel with child world position
        private bool AddVoxel(string voxelName, GameObject go, Transform parent)
        {
            if (go != null)
                return AddVoxel(voxelName, go, parent, GetMapPosition(go.transform.position));
            return false;
        }

        Point SolveMapSize(PointBounds bounds)
        {
            return bounds.max - bounds.min + Point.one;
        }

        //Solve map bounds from a list of child positions
        PointBounds SolveMapBounds(List<Vector3> positions)
        {
            PointBounds bounds = new PointBounds();

            if (positions != null && positions.Count > 0)
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                //Find min and max world positions
                foreach (Vector3 pos in positions)
                {
                    if (pos.x < min.x)
                        min.x = pos.x;
                    if (pos.y < min.y)
                        min.y = pos.y;
                    if (pos.z < min.z)
                        min.z = pos.z;

                    if (pos.x > max.x)
                        max.x = pos.x;
                    if (pos.y > max.y)
                        max.y = pos.y;
                    if (pos.z > max.z)
                        max.z = pos.z;
                }

                //Change min / max world positions into map positions
                bounds.min = GetMapPosition(min);
                bounds.max = GetMapPosition(max);
                bounds.size = SolveMapSize(bounds);
            }

            return bounds;
        }

        //Set neighbours for each voxel in all six directions
        void GenerateNeigbours()
        {
            if (map == null)
                return;

            int iForward = 0;
            int iRight = 0;
            int iBack = 0;
            int iLeft = 0;
            int iUp = 0;
            int iDown = 0;

            Voxel forwardVox = null;
            Voxel rightVox = null;
            Voxel backVox = null;
            Voxel leftVox = null;
            Voxel upVox = null;
            Voxel downVox = null;

            int zLength = map.GetLength(2);
            int yLength = map.GetLength(1);
            int xLength = map.GetLength(0);

            //Iterate all voxels
            for (int z = 0; z < zLength; z++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    for (int x = 0; x < xLength; x++)
                    {
                        if (map[x, y, z] == null)
                            continue;

                        //Neighbour indexes
                        iForward = z + 1;
                        iRight = x + 1;
                        iBack = z - 1;
                        iLeft = x - 1;
                        iUp = y + 1;
                        iDown = y - 1;

                        //Get neigbouring voxels if indexes inside map
                        forwardVox = iForward < zLength ? map[x, y, iForward] : null;
                        rightVox = iRight < xLength ? map[iRight, y, z] : null;
                        backVox = iBack >= 0 ? map[x, y, iBack] : null;
                        leftVox = iLeft >= 0 ? map[iLeft, y, z] : null;
                        upVox = iUp < yLength ? map[x, iUp, z] : null;
                        downVox = iDown >= 0 ? map[x, iDown, z] : null;

                        map[x, y, z].SetNeighbours(forwardVox, rightVox, backVox, leftVox, upVox, downVox);
                    }
                }
            }
        }

        //Reset all agent navigation nodes in the map
        public void ResetNavNodes()
        {
            int zLength = map.GetLength(2);
            int yLength = map.GetLength(1);
            int xLength = map.GetLength(0);

            for (int z = 0; z < zLength; z++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    for (int x = 0; x < xLength; x++)
                        map[x, y, z].navNode.Reset();
                }
            }
        }

        //-------------------------------------------------
        //PUBLIC UTILITY METHODS
        //-------------------------------------------------

        public Voxel GetVoxel(int x, int y, int z)
        {
            if (map == null)
                return null;
            if (x < 0 || x >= map.GetLength(0) || y < 0 || y >= map.GetLength(1) || z < 0 || z >= map.GetLength(2))
                return null;
            return map[x, y, z];
        }

        public Voxel GetVoxel(Point position)
        {
            return GetVoxel(position.x, position.y, position.z);
        }

        //Get voxel with world position
        public Voxel GetVoxel(Vector3 position)
        {
            return GetVoxel(GetMapPosition(position));
        }

        public GameObject GetVoxelChild(Point position)
        {
            Voxel vox = GetVoxel(position);
            if (vox != null && vox.child != null)
                return vox.child;

            return null;
        }

        //Get voxel child with world position
        public GameObject GetVoxelChild(Vector3 position)
        {
            return GetVoxelChild(GetMapPosition(position));
        }

        public Voxel EnterVoxel(VoxelBody body)
        {
            if (body == null)
                return null;

            Voxel vox = GetVoxel(body.Position);
            if (vox != null)
                vox.EnterVoxel(body);
            return vox;
        }

        public void ExitVoxel(VoxelBody body)
        {
            if (body == null)
                return;

            if (body.CurrentVoxel != null)
                body.CurrentVoxel.ExitVoxel(body);
        }

        //Get map position with world position
        public Point GetMapPosition(Vector3 position)
        {
            return new Point(Mathf.RoundToInt((position.x - transform.position.x - mapLocalOffset.x) / unitSize.x),
                Mathf.RoundToInt((position.y - transform.position.y - mapLocalOffset.y) / unitSize.y),
                Mathf.RoundToInt((position.z - transform.position.z - mapLocalOffset.z) / unitSize.z));
        }

        //Get world position with map position
        public Vector3 GetWorldPosition(Point position)
        {
            return new Vector3((float)position.x * unitSize.x + mapLocalOffset.x + transform.position.x,
                (float)position.y * unitSize.y + mapLocalOffset.y + transform.position.y,
                (float)position.z * unitSize.z + mapLocalOffset.z + transform.position.z);
        }

        public Vector3 GetMapWorldOrigin()
        {
            return transform.position + mapLocalOffset;
        }

        public Vector3 GetMapWorldSize()
        {
            return new Vector3(mapBounds.size.x * unitSize.x, mapBounds.size.y * unitSize.y, mapBounds.size.z * unitSize.z);
        }

        public Vector3 GetMapWorldCenter()
        {
            return GetMapWorldOrigin() + (GetMapWorldSize() / 2f) - (unitSize / 2f);
        }

        public bool CheckForSolidVoxel(Point position, LayerMask solidMask)
        {
            return GetVoxel(position).IsSolid(solidMask);
        }

        public bool CheckForSolidVoxel(Point position, LayerMask solidMask, out Voxel voxel)
        {
            voxel = GetVoxel(position);
            if (voxel != null && voxel.child != null && voxel.child.activeInHierarchy == true)
            {
                //Found something. Check if solid
                if ((1 << voxel.child.layer & solidMask.value) > 0)
                    return true;
            }
            return false;
        }

        //-------------------------------------------------
        //DEBUG METHODS
        //-------------------------------------------------

        private void OnDrawGizmos()
        {
            if (debug)
            {
                //Draw map bounds
                Gizmos.color = debugBoundsColor;
                Vector3 realSize = new Vector3(mapBounds.size.x * unitSize.x, mapBounds.size.y * unitSize.y, mapBounds.size.z * unitSize.z);
                Gizmos.DrawWireCube(GetMapWorldOrigin() + ((realSize - unitSize) / 2f), realSize);

                //Draw map origin voxel
                Gizmos.color = debugOriginColor;
                Gizmos.DrawWireCube(GetMapWorldOrigin(), unitSize);

                //Draw right axis
                Gizmos.color = Color.red;
                Gizmos.DrawLine(GetMapWorldOrigin(), transform.position + mapLocalOffset + Vector3.right * unitSize.x);

                //Draw up axis
                Gizmos.color = Color.green;
                Gizmos.DrawLine(GetMapWorldOrigin(), transform.position + mapLocalOffset + Vector3.up * unitSize.y);

                //Draw forward axis
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(GetMapWorldOrigin(), transform.position + mapLocalOffset + Vector3.forward * unitSize.z);
            }
        }

        //-------------------------------------------------
        //NESTED CONTENT
        //-------------------------------------------------

        //Integer based bounds struct
        public struct PointBounds
        {
            public Point min;
            public Point max;
            public Point size;

            public PointBounds(Point min, Point max, Point size)
            {
                this.min = min;
                this.max = max;
                this.size = size;
            }

            public bool IsPointInsideBounds(Point position)
            {
                if (position.x >= 0 && position.x <= size.x && position.y >= 0 && position.y <= size.y && position.z >= 0 && position.z <= size.z)
                    return true;
                return false;
            }
        }
    }
}