﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//The VoxelNavAgent component calculates paths in the VoxelMap and controls a VoxelBody along them
//Requires a VoxelBody component and a VoxelMap object in the scene with some level element children
//Use SetDestination methods to find paths and start moving
//Enable autoUpdate to begin moving after any SetDestination is called, or move manually by calling UpdateVoxelBody
//When VoxelNavAgents collide with eachother the ones with higher priorities wait while others reroute or get out of the way

namespace VoxelMovement
{
    public class VoxelNavAgent : MonoBehaviour
    {
        public static int availablePriority = 0;    //Stores current available agent movement priority value

        public VoxelBody voxelBody;

        //Motion related values
        [Header("Motion")]
        public AnimationCurve movementCurve;        //Default animation curve for movement. Y-axis is transition (0-1), X-axis is time
        public AnimationCurve rotationCurve;        //Default animation curve for rotation. Y-axis is transition (0-1), X-axis is time

        //Navigation related values
        [Header("Navigation")]
        public LayerMask avoidLayers;               //Use this to define which Voxels and VoxelBodies the agent should avoid. For example solid walls and solid players or enemies
        public bool autoUpdate = true;              //Enable to navigate automatically to destination. Disable and call UpdateVoxelBody manually
        public bool autoReRoute = true;             //Should the agent reroute automatically if it's path is interrupted
        public int stoppingDistance = 0;            //Stop within this distance to destination. Value is in map space
        public bool allowPushing = false;           //Allow the agent to push other VoxelBodies

        private Voxel destination;
        private VoxelNavigation.VoxelNavPath path;
        private int pathIndex = 0;
        private bool running = false;
        private bool updateBody = false;

        //Agent movement priority values
        [Header("Agent movement priority")]
        public bool autoSetPriority = true;         //Set agent movement priority automatically to the next available value. When colliding, agents with higher priority wait while others re-route if needed
        public int priority = 0;                    //Value for setting movement priority manually

        //Debug values
        [Header("Debug")]
        public bool debug = false;
        public Color debugColor = Color.white;

        //Events
        [Header("Events")]
        public UnityEvent OnPathCompletedUE;
        public UnityEvent OnPathInterruptedUE;

        public System.Action OnPathCompleted;
        public System.Action OnPathInterrupted;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public Voxel Destination { get { return destination; } }

        public bool Running { get { return running; } }

        public VoxelNavigation.VoxelNavPath Path { get { return path; } }

        //-----------------------------------------------
        //MONOBEHAVIOUR & CUSTOM RESET
        //-----------------------------------------------

        private void OnValidate()
        {
            if (stoppingDistance < 0)
                stoppingDistance = 0;
            if (priority < 0)
                priority = 0;
        }

        private void Awake()
        {
            //Set agent movement priority automatically
            if (autoSetPriority)
            {
                priority = availablePriority;
                availablePriority++;
            }

            if (voxelBody != null)
                voxelBody.agent = this;
        }

        private void Start()
        {
            //Assign VoxelBody events
            voxelBody.OnFailedMoving += OnFailedMoving;
            voxelBody.OnFinishedMoving += OnFinishedMoving;
            voxelBody.OnStoppedRotation += OnStoppedRotation;
            voxelBody.OnEnterVoxelCollision += OnEnterVoxelCollision;
        }

        private void Update()
        {
            if (updateBody)
            {
                updateBody = false;
                UpdateVoxelBody();
            }
        }

        public void ResetThis()
        {
            destination = null;
            pathIndex = 0;
            path = null;
            running = false;
            updateBody = false;
        }

        //-----------------------------------------------
        //PATH SETTING METHODS
        //-----------------------------------------------

        //Set destination with world space position and calculate path
        public bool SetDestination(Vector3 position)
        {
            Voxel destVoxel = VoxelMap.InstanceNoSearch.GetVoxel(position);
            if (destVoxel == null)
                return false;
            return SetDestination(destVoxel);
        }

        //Set destination with map space position and calculate path
        public bool SetDestination(Point position)
        {
            if (VoxelMap.InstanceNoSearch == null)
                return false;
            return SetDestination(VoxelMap.InstanceNoSearch.GetVoxel(position));
        }

        //Set destination with Voxel and calculate path
        public bool SetDestination(Voxel newDestination)
        {
            return SetPath(GetPathTo(newDestination), newDestination);
        }

        public bool SetPath(VoxelNavigation.VoxelNavPath newPath, Point newDestination)
        {
            return SetPath(newPath, VoxelMap.InstanceNoSearch.GetVoxel(newDestination));
        }

        public bool SetPath(VoxelNavigation.VoxelNavPath newPath, Voxel newDestination)
        {
            destination = newDestination;
            path = newPath;
            pathIndex = 0;

            if (path == null)
            {
                running = false;
                updateBody = false;
                return false;
            }

            running = true;

            //Debug draw the path
            if (debug)
            {
                for (int i = 1; i < path.points.Length; i++)
                    Debug.DrawLine(path.points[i - 1] + VoxelMap.InstanceNoSearch.GetMapWorldOrigin(), path.points[i] + VoxelMap.InstanceNoSearch.GetMapWorldOrigin(), debugColor, 3f);
            }

            if (autoUpdate)
                updateBody = true;

            return true;
        }

        //-----------------------------------------------
        //NAVIGATION METHODS
        //-----------------------------------------------

        //Move VoxelBody along the path. Call this method manually if autoUpdate is disabled
        public void UpdateVoxelBody()
        {
            if (path != null)
            {
                //If we haven't reached the last path node yet
                if (pathIndex < (path.points.Length - (1 + stoppingDistance)))
                {
                    Point delta = path.points[pathIndex + 1] - path.points[pathIndex];
                    Point relativeDir = voxelBody.RelativeDirection(delta);

                    //If next movement is forward relative to VoxelBody
                    if (relativeDir.z > 0)
                    {
                        if (autoReRoute)
                            AutoNavigate();
                        else
                            voxelBody.AddMovement(voxelBody.Forward, movementCurve);
                    }
                    //Else it's a rotation
                    else
                        AddRelativeRotation(relativeDir);

                    return;
                }
                //If reached non-zero stopping distance then turn towards the destination
                else if (pathIndex < path.points.Length - 1)
                {
                    Point destinationDir = (destination.position - voxelBody.Position);
                    Point relativeDir = voxelBody.RelativeDirection(destinationDir);

                    Voxel.Directions direction;

                    if (GetLongestDirection(relativeDir, out direction))
                    {
                        if (AddRelativeRotation(direction))
                            return;
                    }
                }
            }

            //Otherwise we are at the end of the path. Stop agent from running
            path = null;
            pathIndex = 0;
            running = false;

            //Trigger events
            if (debug)
                Debug.Log(gameObject.name + ": Path completed!");
            if (OnPathCompleted != null)
                OnPathCompleted();
            OnPathCompletedUE.Invoke();
        }

        //Detect obstacles and navigate them
        private void AutoNavigate()
        {
            //Check if there is another agent in front
            VoxelNavAgent immediateCollidingAgent = GetImmediateCollision();

            //Check if there are other agents adjacent to the destination and about to move into the destination
            List<VoxelNavAgent> collidingNavAgents = GetPotentialOutOfWayCollisions();

            //If there is a potential collision with a voxel or another agent
            if (VoxelMap.Instance.CheckForSolidVoxel(path.points[pathIndex + 1], voxelBody.solidLayers) || immediateCollidingAgent != null || collidingNavAgents != null)
            {
                //If there is another agent in front and it has a lower move priority, then wait
                if (immediateCollidingAgent != null)
                {
                    if (immediateCollidingAgent.running && priority > immediateCollidingAgent.priority)
                    {
                        Invoke("Wait", 3f);
                        return;
                    }
                }
                //If there are adjacent agents to the target about to move into it, and they have a higher move priority then wait. Otherwise pass
                else if (collidingNavAgents != null)
                {
                    foreach (VoxelNavAgent otherAgent in collidingNavAgents)
                    {
                        if (otherAgent.running && priority < otherAgent.priority)
                        {
                            Invoke("Wait", 1f);
                            return;
                        }
                    }

                    voxelBody.AddMovement(voxelBody.Forward, movementCurve);
                    return;
                }

                //Otherwise reroute
                VoxelNavigation.VoxelNavPath newPath = GetPathTo(destination);

                //If cannot re-route then check if can push body to next spot
                if (newPath == null && allowPushing)
                {
                    voxelBody.AddMovement(voxelBody.Forward, movementCurve);
                }
                //Else take new path
                else
                {
                    if (SetPath(newPath, destination))
                    {
                        if (debug)
                            Debug.Log(gameObject.name + ": Re-pathing");
                    }
                    else
                    {
                        if (debug)
                            Debug.Log(gameObject.name + ": Path interrupted. Obstacle in path");
                        if (OnPathInterrupted != null)
                            OnPathInterrupted();
                        OnPathInterruptedUE.Invoke();
                    }
                }
            }
            //If there's no collision then move
            else
                voxelBody.AddMovement(voxelBody.Forward, movementCurve);
        }

        //Rotate body towards a relative enum direction
        private bool AddRelativeRotation(Voxel.Directions relativeDir)
        {
            switch (relativeDir)
            {
                case Voxel.Directions.RIGHT: voxelBody.AddRotation(voxelBody.Up, rotationCurve); break;
                case Voxel.Directions.LEFT: voxelBody.AddRotation(voxelBody.Up * -1, rotationCurve); break;
                case Voxel.Directions.BACK:
                    if (Random.value > 0.5f)
                        voxelBody.AddRotation(voxelBody.Up * 2, rotationCurve);
                    else
                        voxelBody.AddRotation(voxelBody.Up * -2, rotationCurve);
                    break;
                default:
                    //No need to turn towards relative forward
                    return false;
            }
            return true;
        }

        //Rotate body towards a relative Point direction
        private bool AddRelativeRotation(Point relativeDir)
        {
            Voxel.Directions direction;

            if (GetLongestDirection(relativeDir, out direction))
            {
                if (AddRelativeRotation(direction))
                    return true;
            }
            return false;
        }

        //Wait is used with Invokes and delay
        private void Wait()
        {
            //Attempt to re-route
            if (SetDestination(destination))
            {
                if (debug)
                    Debug.Log(gameObject.name + ": Re-pathing");
            }
            else
            {
                if (debug)
                    Debug.Log(gameObject.name + ": Path interrupted. Obstacle in path");
                if (OnPathInterrupted != null)
                    OnPathInterrupted();
                OnPathInterruptedUE.Invoke();
            }
        }

        //-----------------------------------------------
        //OBSTACLE DETECTION
        //-----------------------------------------------

        //Look for a VoxelNavAgent in front
        private VoxelNavAgent GetImmediateCollision()
        {
            Point nextPosition = voxelBody.Position + voxelBody.Forward;
            List<VoxelBody> bodies = null;

            //Draw the checked position in front of the VoxelBody
            if (debug)
                VoxelNavigation.PaintVoxel(nextPosition, debugColor, movementCurve[movementCurve.length - 1].time);

            //Check for solid VoxelBody with agent
            if (VoxelBodyManager.Instance.CheckForBodies(nextPosition, voxelBody.solidLayers, out bodies))
            {
                foreach (VoxelBody otherBody in bodies)
                {
                    if (otherBody.agent != null && otherBody.agent != this)
                        return otherBody.agent;
                }
            }
            return null;
        }

        //Look for a VoxelNavAgent in a specific map space position and see if it will get in the way
        private VoxelNavAgent GetPotentialCollisionAt(Point nextMovePosition, Point positionToCheck)
        {
            VoxelNavAgent otherAgent = null;
            List<VoxelBody> bodies = null;
            VoxelBodyManager.Instance.CheckForBodies(positionToCheck, voxelBody.solidLayers, out bodies);
            if (bodies != null)
            {
                foreach (VoxelBody otherBody in bodies)
                {
                    otherAgent = otherBody.agent;
                    //Alternatively just use the other agent's path index!
                    if (otherAgent != null)
                    {
                        //Get other agent's nearest path index
                        int truePathIndex = otherAgent.voxelBody.State == VoxelBody.MotionStates.MOVING && otherAgent.voxelBody.MotionTransition >= 0.5f ? otherAgent.pathIndex + 1 : otherAgent.pathIndex;

                        //If the other agent is moving and it's next path node is where we want to move, then return the agent
                        if (otherAgent != null && otherAgent != this && otherAgent.running && truePathIndex < otherAgent.path.points.Length - 1 && otherAgent.path.points[truePathIndex + 1] == nextMovePosition)
                            return otherAgent;
                    }
                }
            }
            return null;
        }

        //Look for VoxelNavAgents in adjacent spots to where this agent's VoxelBody is moving into, and see if they will get in the way
        private List<VoxelNavAgent> GetPotentialOutOfWayCollisions()
        {
            List<VoxelNavAgent> collidingAgents = null;
            VoxelNavAgent otherAgent = null;
            Point nextPosition = voxelBody.Position + voxelBody.Forward;

            //Check the position two steps over in the forward direction for other agents

            Point positionToCheck = voxelBody.Position + voxelBody.Forward * 2;

            //Draw rectangle to that spot
            if (debug)
                VoxelNavigation.PaintVoxel(positionToCheck, debugColor, movementCurve[movementCurve.length - 1].time);

            otherAgent = GetPotentialCollisionAt(nextPosition, positionToCheck);
            if (otherAgent != null)
            {
                if (collidingAgents == null)
                    collidingAgents = new List<VoxelNavAgent>();
                collidingAgents.Add(otherAgent);
            }

            //Check the position diagonally left for other agents

            positionToCheck = voxelBody.Position + voxelBody.Forward - voxelBody.Right;

            //Draw rectangle to that spot
            if (debug)
                VoxelNavigation.PaintVoxel(positionToCheck, debugColor, movementCurve[movementCurve.length - 1].time);

            otherAgent = GetPotentialCollisionAt(nextPosition, positionToCheck);
            if (otherAgent != null)
            {
                if (collidingAgents == null)
                    collidingAgents = new List<VoxelNavAgent>();
                collidingAgents.Add(otherAgent);
            }

            //Check the position diagonally right for other agents

            positionToCheck = voxelBody.Position + voxelBody.Forward + voxelBody.Right;

            //Draw rectangle to that spot
            if (debug)
                VoxelNavigation.PaintVoxel(positionToCheck, debugColor, movementCurve[movementCurve.length - 1].time);

            otherAgent = GetPotentialCollisionAt(nextPosition, positionToCheck);
            if (otherAgent != null)
            {
                if (collidingAgents == null)
                    collidingAgents = new List<VoxelNavAgent>();
                collidingAgents.Add(otherAgent);
            }

            return collidingAgents;
        }

        //-----------------------------------------------
        //UTILITY METHODS
        //-----------------------------------------------

        public VoxelNavigation.VoxelNavPath GetPathTo(Point newDestination)
        {
            return VoxelNavigation.FindPath(voxelBody.CurrentVoxel, VoxelMap.InstanceNoSearch.GetVoxel(newDestination), avoidLayers);
        }

        public VoxelNavigation.VoxelNavPath GetPathTo(Voxel newDestination)
        {
            return VoxelNavigation.FindPath(voxelBody.CurrentVoxel, newDestination, avoidLayers);
        }

        //Method that takes a Point and returns the longest axis as direction enum value
        private bool GetLongestDirection(Point point, out Voxel.Directions direction)
        {
            direction = Voxel.Directions.FORWARD;

            if (point.magnitude > 0)
            {
                //Turn axis values into absolutes. We're only interested in comparing lengths
                int absX = point.x < 0 ? -point.x : point.x;
                int absY = point.y < 0 ? -point.y : point.y;
                int absZ = point.z < 0 ? -point.z : point.z;

                //Compare axis values and set result
                if (absX > absY && absX > absZ)
                    direction = point.x > 0 ? Voxel.Directions.RIGHT : Voxel.Directions.LEFT;
                else if (absY > absX && absY > absZ)
                    direction = point.y > 0 ? Voxel.Directions.UP : Voxel.Directions.DOWN;
                else if (absZ > absX && absZ > absY)
                    direction = point.z > 0 ? Voxel.Directions.FORWARD : Voxel.Directions.BACK;
                else
                    //If there is a tie
                    return false;

                return true;
            }
            else
                //If point magnitude is zero there's no result
                return false;
        }

        //-----------------------------------------------
        //VOXELBODY EVENTS
        //-----------------------------------------------

        //VoxelBody has finished moving one square
        private void OnFinishedMoving(Point position)
        {
            if (debug)
                Debug.Log(gameObject.name + ": On finished moving");

            pathIndex++;

            //If we are set to autoUpdate get ready for next frame
            if (autoUpdate)
                updateBody = true;
            else
            {
                //If we have been moved off path somehow stop the agent and trigger events
                if (voxelBody.Position != path.points[pathIndex])
                {
                    path = null;
                    running = false;
                    if (debug)
                        Debug.Log(gameObject.name + ": Path interrupted. Agent off-path");
                    if (OnPathInterrupted != null)
                        OnPathInterrupted();
                    OnPathInterruptedUE.Invoke();
                }
            }
        }

        //If VoxelBody was prevented from moving by a solid Voxel or another body
        private void OnFailedMoving(Point position)
        {
            if (debug)
                Debug.Log(gameObject.name + ": On failed moving");

            //If we are set to autoReRoute attempt to set another path to destination
            if (autoReRoute)
            {
                if (SetDestination(destination))
                {
                    if (debug)
                        Debug.Log(gameObject.name + ": Re-pathing");
                }
                //If there is no path to destination stop and trigger events
                else
                {
                    if (debug)
                        Debug.Log(gameObject.name + ": Path interrupted. Obstacle in path");
                    if (OnPathInterrupted != null)
                        OnPathInterrupted();
                    OnPathInterruptedUE.Invoke();
                }
            }
            //Else stop and trigger events
            else
            {
                path = null;
                running = false;
                if (debug)
                    Debug.Log(gameObject.name + ": Path interrupted. Obstacle in path");
                if (OnPathInterrupted != null)
                    OnPathInterrupted();
                OnPathInterruptedUE.Invoke();
            }
        }

        //VoxelBody has finished rotation
        private void OnStoppedRotation(Point rotation)
        {
            //If we are set to autoUpdate get ready for next frame
            if (autoUpdate)
                updateBody = true;
        }

        //VoxelBody has collided with a solid Voxel
        private void OnEnterVoxelCollision(Voxel voxel)
        {
            if (autoReRoute)
            {
                //If body ran into a collision and was stopped
                if (running && voxelBody.State == VoxelBody.MotionStates.STOPPED)
                {
                    //Attempt to set another path to destination
                    if (SetDestination(destination))
                    {
                        if (debug)
                            Debug.Log(gameObject.name + ": Re-pathing");
                    }
                    //If there is no path to destination stop and trigger events
                    else
                    {
                        if (debug)
                            Debug.Log(gameObject.name + ": Path interrupted. Voxel in path");
                        if (OnPathInterrupted != null)
                            OnPathInterrupted();
                        OnPathInterruptedUE.Invoke();
                    }
                }
            }
            //If agent was not set to autoReRoute, stop and trigger events
            else
            {
                path = null;
                running = false;
                if (debug)
                    Debug.Log(gameObject.name + ": Path interrupted. Voxel in path");
                if (OnPathInterrupted != null)
                    OnPathInterrupted();
                OnPathInterruptedUE.Invoke();
            }
        }
    }
}