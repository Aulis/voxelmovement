﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Simple script for grouping level elements when placing them inside Voxels. Can be used to define rooms for example.

namespace VoxelMovement
{
    public class VoxelGroup : MonoBehaviour
    {
        public Transform groupTransform;
    }
}
