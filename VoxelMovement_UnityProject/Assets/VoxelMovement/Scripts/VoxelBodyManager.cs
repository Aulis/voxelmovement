﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//VoxelBodyManager updates VoxelBody motions and collisions
//The class is a monobehaviour singleton. Only a single instance per scene is required
//Requires VoxelMap to be in the scene.

namespace VoxelMovement
{
    public class VoxelBodyManager : MonoBehaviour
    {

        private static VoxelBodyManager instance;

        //VoxelBody related values
        private List<VoxelBody> bodies = new List<VoxelBody>();
        private List<VoxelBody> bodiesToAdd = new List<VoxelBody>();
        private List<VoxelBody> bodiesToRemove = new List<VoxelBody>();
        private List<VoxelBody> bodiesToStop = new List<VoxelBody>();
        private Dictionary<VoxelBody, PushEntry> pushEntries = new Dictionary<VoxelBody, PushEntry>();

        //Debug
        public bool debugPrint = false;
        public KeyCode debugPrintButton = KeyCode.P;    //Enable debug and press the debugPrintButton to print body list contents

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public static VoxelBodyManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType(typeof(VoxelBodyManager)) as VoxelBodyManager;

                    //If manager doesn't exist in scene create new one
                    if (instance == null)
                    {
                        GameObject go = new GameObject("VoxelBodyManager");
                        instance = go.AddComponent<VoxelBodyManager>();
                    }
                }
                return instance;
            }
        }

        public static VoxelBodyManager InstanceNoCreate { get { return instance; } }

        //-----------------------------------------------
        //MONOBEHAVIOUR
        //-----------------------------------------------

        private void Update()
        {
            if (debugPrint)
            {
                if (Input.GetKeyDown(debugPrintButton))
                    DebugPrintLists();
            }
        }

        private void FixedUpdate()
        {
            AddNewBodies();
            RemoveBodies();
            UpdateVoxelBodies();
        }

        //-----------------------------------------------
        //PUBLIC UTILITY METHODS
        //-----------------------------------------------

        //Get all active bodies occupying a voxel
        public List<VoxelBody> GetBodiesInPosition(Point position)
        {
            Voxel vox = VoxelMap.Instance.GetVoxel(position);
            List<VoxelBody> foundBodies = null;

            if (vox == null)
                return null;

            foreach (VoxelBody body in vox.Bodies)
            {
                if (body == null)
                    continue;

                if (body.isActiveAndEnabled == true && body.isBeingRemoved == false)
                {
                    if (foundBodies == null)
                        foundBodies = new List<VoxelBody>();
                    foundBodies.Add(body);
                }
            }

            return foundBodies;
        }

        //Get all active bodies in voxel and voxel child
        public List<GameObject> GetGameObjectsInPosition(Point position)
        {
            Voxel vox = VoxelMap.Instance.GetVoxel(position);
            List<GameObject> foundObjects = new List<GameObject>();

            if (vox != null)
            {
                foreach (VoxelBody body in vox.Bodies)
                {
                    if (body == null)
                        continue;

                    if (body.isActiveAndEnabled == true && body.isBeingRemoved == false)
                        foundObjects.Add(body.gameObject);
                }

                GameObject voxelChild = vox.child;
                if (voxelChild != null)
                    foundObjects.Add(voxelChild);
            }

            return foundObjects;
        }

        //-----------------------------------------------
        //BODY ADD & REMOVE METHODS
        //-----------------------------------------------

        //Bodies call this method to add themselves to manager
        public void AddVoxelBody(VoxelBody body)
        {
            if (body == null)
                return;
            if (bodiesToAdd.Contains(body))
                return;
            if (bodies.Contains(body))
                return;

            bodiesToAdd.Add(body);
        }

        //Bodies call this method to remove themselves from manager
        public void RemoveVoxelBody(VoxelBody body)
        {
            if (body == null)
                return;
            if (bodiesToRemove.Contains(body))
                return;
            if (bodies.Contains(body) == false)
                return;

            body.isBeingRemoved = true;
            bodiesToRemove.Add(body);
        }

        //Add all new bodies and clear the list
        private void AddNewBodies()
        {
            foreach (VoxelBody body in bodiesToAdd)
                bodies.Add(body);

            bodiesToAdd.Clear();
        }

        //Remove all bodies set to be removed and clear the list
        private void RemoveBodies()
        {
            foreach (VoxelBody body in bodiesToRemove)
            {
                body.isBeingRemoved = false;
                bodies.Remove(body);
            }
            bodiesToRemove.Clear();
        }

        //-----------------------------------------------
        //BODY UPDATE METHODS
        //-----------------------------------------------

        private void UpdateVoxelBodies()
        {
            //Only update bodies if map exists
            if (VoxelMap.InstanceNoSearch == null)
                return;

            //Call bodies to update their movement and rotation
            foreach (VoxelBody body in bodies)
                body.UpdateMotion();

            UpdateBodyToMapCollisions();
            UpdateBodyToBodyCollisions();
            ResolveCollisions();
        }

        //Check for collisions between bodies and map
        private void UpdateBodyToMapCollisions()
        {
            Voxel voxel;

            foreach (VoxelBody body in bodies)
            {
                if (body.isActiveAndEnabled == false || body.isBeingRemoved)
                    continue;

                //If there is a collision move bodies back and stop them
                if (VoxelMap.Instance.CheckForSolidVoxel(body.Position, body.solidLayers, out voxel))
                {
                    body.Position = body.PreviousPosition;
                    body.SetStopped(false);

                    if (body.OnFailedMoving != null)
                        body.OnFailedMoving(body.Position);
                    body.OnFailedMovingUE.Invoke(body.Position);

                    if (body.OnEnterVoxelCollision != null)
                        body.OnEnterVoxelCollision(voxel);
                }
            }
        }

        //Check for collisions between bodies
        private void UpdateBodyToBodyCollisions()
        {
            List<VoxelBody> collidingBodies;
            bool addedToStopList = false;
            bool addedToPushingList = false;

            //Check for body collisions
            foreach (VoxelBody body in bodies)
            {
                if (body.isActiveAndEnabled == false || body.isBeingRemoved)
                    continue;

                //Get bodies currently colliding with this one. If there are none, skip this body
                if (CheckForBodyCollisions(body, out collidingBodies) == false)
                    continue;

                addedToStopList = false;
                addedToPushingList = false;

                foreach (VoxelBody otherBody in collidingBodies)
                {
                    //Add colliding body to list for collision event
                    body.collidingBodies.Add(otherBody);

                    //If either body is a trigger, collision doesn't need to be resolved because they pass through
                    if (body.triggerType || otherBody.triggerType)
                        continue;

                    //If body was moving
                    if (body.State == VoxelBody.MotionStates.MOVING)
                    {
                        //If other body was moving
                        if (otherBody.State == VoxelBody.MotionStates.MOVING)
                        {
                            //If other body was further with movement, add this body to be stopped
                            if (body.MotionTransition <= otherBody.MotionTransition && addedToStopList == false)
                            {
                                bodiesToStop.Add(body);
                                addedToStopList = true;
                            }
                        }
                        //If other body was stopped
                        else if (otherBody.State == VoxelBody.MotionStates.STOPPED || otherBody.State == VoxelBody.MotionStates.ROTATING)
                        {
                            //If this body can push other body, update push entry
                            if (body.canPush && otherBody.pushable && addedToPushingList == false)
                            {
                                if (pushEntries.ContainsKey(otherBody))
                                    pushEntries[otherBody].pushingBodies.Add(body);
                                else
                                    pushEntries.Add(otherBody, new PushEntry(body));
                                addedToPushingList = true;
                            }
                            //If not, add this body to be stopped
                            else if (addedToStopList == false)
                            {
                                bodiesToStop.Add(body);
                                addedToStopList = true;
                            }
                        }
                    }
                    //If body was stopped AND other body was moving
                    else if (otherBody.State == VoxelBody.MotionStates.MOVING)
                    {
                        //If can be pushed by other body, update push entry
                        if (body.pushable && otherBody.canPush)
                        {
                            if (pushEntries.ContainsKey(body) == false)
                                pushEntries.Add(body, new PushEntry());
                        }
                    }
                }
            }
        }

        private void ResolveCollisions()
        {
            //Handle the list of bodies to stop
            if (bodiesToStop.Count > 0)
            {
                foreach (VoxelBody body in bodiesToStop)
                {
                    //If body was moving, set it back and stop it
                    if (body.State == VoxelBody.MotionStates.MOVING && body.isActiveAndEnabled && body.isBeingRemoved == false)
                    {
                        body.Position = body.PreviousPosition;
                        body.SetStopped(false);
                        if (body.OnFailedMoving != null)
                            body.OnFailedMoving(body.Position);
                        body.OnFailedMovingUE.Invoke(body.Position);
                    }
                }
                bodiesToStop.Clear();
            }

            //Handle the list of bodies to push
            if (pushEntries.Count > 0)
            {
                foreach (KeyValuePair<VoxelBody, PushEntry> entry in pushEntries)
                {
                    //Proceed with push if there's only one pusher
                    if (entry.Value.pushingBodies.Count == 1)
                    {
                        VoxelBody pusher = entry.Value.pushingBodies[0];
                        //If the push direction is clear, then move
                        if (CheckForSolid(entry.Key.Position + pusher.MovementDirection, entry.Key.solidLayers) == false)
                        {
                            //Set pushable body to move immediately
                            entry.Key.SetState(entry.Key.Position + pusher.MovementDirection, entry.Key.Position, entry.Key.Rotation, entry.Key.Rotation, 0f, pusher.MovementDirection, VoxelBody.MotionStates.MOVING, pusher.movementCurve, pusher.rotationCurve);
                            entry.Value.succeeded = true;
                        }
                    }

                    //If there were more than one pusher, or the push entry failed, then stop all pushing bodies
                    if (entry.Value.succeeded == false)
                    {
                        foreach (VoxelBody body in entry.Value.pushingBodies)
                        {
                            body.Position = body.PreviousPosition;
                            body.SetStopped(false);
                            if (body.OnFailedMoving != null)
                                body.OnFailedMoving(body.Position);
                            body.OnFailedMovingUE.Invoke(body.Position);
                        }
                    }
                }
                pushEntries.Clear();
            }

            //Trigger collision events
            foreach (VoxelBody body in bodies)
            {
                if (body.collidingBodies.Count > 0)
                {
                    if (body.OnEnterBodyCollision != null)
                        body.OnEnterBodyCollision(body.collidingBodies);
                    body.collidingBodies.Clear();
                }
            }
        }

        //-----------------------------------------------
        //COLLISION CHECK METHODS
        //-----------------------------------------------

        public bool CheckForBodyCollision(VoxelBody bodyA, VoxelBody bodyB)
        {
            //If bodies overlap
            if (bodyA.Position == bodyB.Position)
            {
                //If body layers can't collide, either is disabled or if either is being removed, then there is no collision
                if (((1 << bodyB.gameObject.layer & bodyA.solidLayers.value) > 0) == false || bodyA.isActiveAndEnabled == false || bodyB.isActiveAndEnabled == false || bodyA.isBeingRemoved == true || bodyB.isBeingRemoved == true)
                    return false;

                //If either is stopped then there is a collision
                if ((bodyA.State == VoxelBody.MotionStates.STOPPED || bodyA.State == VoxelBody.MotionStates.ROTATING) || (bodyB.State == VoxelBody.MotionStates.STOPPED || bodyB.State == VoxelBody.MotionStates.ROTATING))
                    return true;
                else
                {
                    //If both are moving then collide if Transform position is withing square
                    if (bodyA.MotionTransition > 0.5f && bodyA.MotionTransition < 1.5f || bodyB.MotionTransition > 0.5f && bodyB.MotionTransition < 1.5f)
                        return true;
                }
            }
            return false;
        }

        public bool CheckForBodies(Point position, LayerMask solidMask)
        {
            //Get bodies in the position
            List<VoxelBody> foundBodies = GetBodiesInPosition(position);

            if (foundBodies == null)
                return false;

            foreach (VoxelBody body in foundBodies)
            {
                //If the body is solid
                if ((1 << body.gameObject.layer & solidMask.value) > 0)
                {
                    //Return true if the body is not moving, or the Transform is within square
                    if (body.State == VoxelBody.MotionStates.STOPPED || body.State == VoxelBody.MotionStates.ROTATING)
                        return true;
                    else if (body.MotionTransition > 0.5f && body.MotionTransition < 1.5f)
                        return true;
                }
            }
            return false;
        }

        public bool CheckForBodies(Point position, LayerMask solidMask, out List<VoxelBody> bodies)
        {
            //Get bodies in the position
            bodies = null;
            List<VoxelBody> foundBodies = GetBodiesInPosition(position);

            if (foundBodies == null)
                return false;

            foreach (VoxelBody body in foundBodies)
            {
                //If the body is solid
                if ((1 << body.gameObject.layer & solidMask.value) > 0)
                {
                    //Return true if the body is not moving
                    if (body.State == VoxelBody.MotionStates.STOPPED || body.State == VoxelBody.MotionStates.ROTATING)
                    {
                        if (bodies == null)
                            bodies = new List<VoxelBody>();
                        bodies.Add(body);
                    }
                    //Or if the Transform is within square
                    else if (body.MotionTransition > 0.5f && body.MotionTransition < 1.5f)
                    {
                        if (bodies == null)
                            bodies = new List<VoxelBody>();
                        bodies.Add(body);
                    }
                }
            }

            if (bodies != null && bodies.Count > 0)
                return true;
            return false;
        }

        public bool CheckForBodyCollisions(VoxelBody body, out List<VoxelBody> bodies)
        {
            bodies = null;
            if (body.isBeingRemoved)
                return false;

            //Handle target position
            List<VoxelBody> targetPosBodies = GetBodiesInPosition(body.Position);

            if (targetPosBodies != null)
            {
                foreach (VoxelBody otherBody in targetPosBodies)
                {
                    //If the other body is solid add it to the list
                    if (otherBody != body && ((1 << otherBody.gameObject.layer & body.solidLayers.value) > 0))
                    {
                        if (bodies == null)
                            bodies = new List<VoxelBody>();
                        bodies.Add(otherBody);
                    }
                }
            }

            //Handle head-on collisions. BodyA is moving to BodyB & BodyB is moving to BodyA
            if (body.State == VoxelBody.MotionStates.MOVING)
            {
                targetPosBodies = GetBodiesInPosition(body.PreviousPosition);

                if (targetPosBodies != null)
                {
                    foreach (VoxelBody otherBody in targetPosBodies)
                    {
                        //If the other body is solid
                        if (otherBody != body && ((1 << otherBody.gameObject.layer & body.solidLayers.value) > 0))
                        {
                            //If the other body is passing through this body add it to the list
                            if (otherBody.State == VoxelBody.MotionStates.MOVING && otherBody.PreviousPosition == body.Position)
                            {
                                if (bodies == null)
                                    bodies = new List<VoxelBody>();
                                bodies.Add(otherBody);
                            }
                        }
                    }
                }
            }

            if (bodies != null && bodies.Count > 0)
                return true;
            return false;
        }

        //Check for solid voxels or bodies in position
        public bool CheckForSolid(Point position, LayerMask solidMask)
        {
            if (VoxelMap.Instance.CheckForSolidVoxel(position, solidMask))
                return true;

            List<VoxelBody> foundBodies = GetBodiesInPosition(position);

            if (foundBodies == null)
                return false;

            foreach (VoxelBody body in foundBodies)
            {
                if (body.IsSolid(solidMask))
                    return true;
            }

            return false;
        }

        //Check for bodies whose finebounds overlap offered worldspace bounds
        public bool FineCheckForBodies(Vector3 position, Bounds bounds, LayerMask solidLayers, out List<VoxelBody> bodies)
        {
            bodies = null;

            //Convert worldspace position to map position
            Point pointStart = VoxelMap.Instance.GetMapPosition(position);
            Point pointDelta = new Point();

            //Only check if position is inside map
            if (VoxelMap.Instance.MapBounds.IsPointInsideBounds(pointStart))
            {
                List<VoxelBody> bodiesInPosition;
                Bounds boundsA = new Bounds();
                Bounds boundsB = new Bounds();

                for (int i = 0; i < 9; i++)
                {
                    //A body in movement can overlap nine squares around the position. Look for bodies around the starting point and check each with fine bounds
                    switch (i)
                    {
                        case 0: pointDelta = Point.zero; break;
                        case 1: pointDelta = Point.forward; break;
                        case 2: pointDelta = Point.forward + Point.right; break;
                        case 3: pointDelta = Point.right; break;
                        case 4: pointDelta = -Point.forward + Point.right; break;
                        case 5: pointDelta = -Point.forward; break;
                        case 6: pointDelta = -Point.forward - Point.right; break;
                        case 7: pointDelta = -Point.right; break;
                        case 8: pointDelta = Point.forward - Point.right; break;
                    }

                    bodiesInPosition = GetBodiesInPosition(pointStart + pointDelta);

                    if (bodiesInPosition != null)
                    {
                        foreach (VoxelBody body in bodiesInPosition)
                        {
                            if (body.IsSolid(solidLayers))
                            {
                                //If the body's finebounds intersect the offered bounds then add them to the list
                                boundsA.center = body.transform.position + body.fineBounds.center;
                                boundsA.extents = body.fineBounds.extents;
                                boundsB.center = position + bounds.center;
                                boundsB.extents = bounds.extents;
                                if (boundsA.Intersects(boundsB))
                                {
                                    if (bodies == null)
                                        bodies = new List<VoxelBody>();
                                    bodies.Add(body);
                                }
                            }
                        }
                    }
                }

                if (bodies != null && bodies.Count > 0)
                    return true;
                return false;
            }
            else
                return false;
        }

        //----------------------------------------------
        //DEBUG METHODS
        //----------------------------------------------

        //Print the contents of all body lists
        public void DebugPrintLists()
        {
            Debug.Log("------------ VoxelBodyManager Content ------------");
            string message = "Bodies: ";
            foreach (VoxelBody body in bodies)
                message += body.gameObject.name + ", ";
            Debug.Log(message);
            message = "Bodies to Add: ";
            foreach (VoxelBody body in bodiesToAdd)
                message += body.gameObject.name + ", ";
            Debug.Log(message);
            message = "Bodies to remove: ";
            foreach (VoxelBody body in bodiesToRemove)
                message += body.gameObject.name + ", ";
            Debug.Log(message);
            message = "Bodies to resolve: ";
            foreach (VoxelBody body in bodiesToStop)
                message += body.gameObject.name + ", ";
            Debug.Log(message);
            Debug.Log("--------------------------------------------------");
        }

        //----------------------------------------------
        //NESTED CLASSES
        //----------------------------------------------

        //A helper class for push events. Push entry lists which VoxelBodies are pushing a certain VoxelBody and whether the push succeeded or not
        class PushEntry
        {
            public List<VoxelBody> pushingBodies = new List<VoxelBody>();
            public bool succeeded = false;

            public PushEntry() { }

            public PushEntry(VoxelBody body)
            {
                pushingBodies.Add(body);
            }
        }
    }
}