﻿using UnityEngine;
using System.Collections;

//Integer 3D vector struct, based largely on Unity's Vector3 struct
//Used largely by the voxel map and movement system

namespace VoxelMovement
{
    [System.Serializable]
    public struct Point
    {
        public int x;
        public int y;
        public int z;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public static Point zero { get { return new Point(0, 0, 0); } }
        public static Point one { get { return new Point(1, 1, 1); } }
        public static Point forward { get { return new Point(0, 0, 1); } }
        public static Point up { get { return new Point(0, 1, 0); } }
        public static Point right { get { return new Point(1, 0, 0); } }

        public float magnitude { get { return Mathf.Sqrt((float)((x * x) + (y * y) + (z * z))); } }
        public float sqrMagnitude { get { return (x * x) + (y * y) + (z * z); } }

        public Point clampedToUnit { get { return new Point(Mathf.Clamp(x, -1, 1), Mathf.Clamp(y, -1, 1), Mathf.Clamp(z, -1, 1)); } }

        //----------------------------------------------
        //CONSTRUCTORS
        //----------------------------------------------

        public Point(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        //----------------------------------------------
        //OPERATOR OVERLOADING
        //----------------------------------------------

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Point operator +(Point a, Vector3 b)
        {
            return new Point(Mathf.RoundToInt(a.x + b.x), Mathf.RoundToInt(a.y + b.y), Mathf.RoundToInt(a.z + b.z));
        }

        public static Point operator +(Vector3 a, Point b)
        {
            return new Point(Mathf.RoundToInt(a.x + b.x), Mathf.RoundToInt(a.y + b.y), Mathf.RoundToInt(a.z + b.z));
        }

        public static Point operator -(Point a, Point b)
        {
            return new Point(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static Point operator -(Point a, Vector3 b)
        {
            return new Point(Mathf.RoundToInt(a.x - b.x), Mathf.RoundToInt(a.y - b.y), Mathf.RoundToInt(a.z - b.z));
        }

        public static Point operator -(Vector3 a, Point b)
        {
            return new Point(Mathf.RoundToInt(a.x - b.x), Mathf.RoundToInt(a.y - b.y), Mathf.RoundToInt(a.z - b.z));
        }

        public static Point operator -(Point a)
        {
            return new Point(-a.x, -a.y, -a.z);
        }

        public static Point operator *(Point a, int b)
        {
            return new Point(a.x * b, a.y * b, a.z * b);
        }

        public static Vector3 operator *(Point a, float b)
        {
            return new Vector3((float)a.x * b, (float)a.y * b, (float)a.z * b);
        }

        public static Vector3 operator *(Point a, Vector3 b)
        {
            return new Vector3((float)a.x * b.x, (float)a.y * b.y, (float)a.z * b.z);
        }

        public static Vector3 operator *(Vector3 a, Point b)
        {
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static bool operator ==(Point lhs, Point rhs)
        {
            if (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z)
                return true;
            return false;
        }

        public static bool operator !=(Point lhs, Point rhs)
        {
            if (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z)
                return false;
            return true;
        }

        public static implicit operator Vector3(Point a)
        {
            return new Vector3(a.x, a.y, a.z);
        }

        public static implicit operator Point(Vector3 a)
        {
            return new Point(Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y), Mathf.RoundToInt(a.z));
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("(" + x + ", " + y + ", " + z + ")");
        }

        //----------------------------------------------
        //ROTATION METHODS
        //----------------------------------------------

        //Helper function for rotating a point in 2D space by 90 degree steps
        private void Rotate2DPoint90(ref int x, ref int y, int amount)
        {
            int temp;
            amount &= 3;

            temp = x;
            switch (amount)
            {
                case 1:
                    x = y;
                    y = -temp;
                    break;
                case 2:
                    x = -x;
                    y = -y;
                    break;
                case 3:
                    x = -y;
                    y = temp;
                    break;
                default: break;
            }
        }

        //Rotate instance by 90 degree steps
        public void Rotate90(int xAmount, int yAmount, int zAmount)
        {
            Rotate2DPoint90(ref z, ref y, xAmount);
            Rotate2DPoint90(ref x, ref z, yAmount);
            Rotate2DPoint90(ref y, ref x, zAmount);
        }

        //Rotate instance by 90 degree steps
        public void Rotate90(Point amount)
        {
            Rotate90(amount.x, amount.y, amount.z);
        }

        //Rotate given Point by 90 degree steps
        public static Point Rotated90(Point point, Point amount)
        {
            Point rotatedPoint = new Point(point.x, point.y, point.z);
            rotatedPoint.Rotate90(amount);
            return rotatedPoint;
        }

        //----------------------------------------------
        //OTHER METHODS
        //----------------------------------------------

        //Clamp instance axis between -1 and 1
        public Point ClampToUnit()
        {
            x = Mathf.Clamp(x, -1, 1);
            y = Mathf.Clamp(y, -1, 1);
            z = Mathf.Clamp(z, -1, 1);
            return this;
        }

        //Clamp given Point axis between -1 and 1
        public static Point ClampToUnit(Point point)
        {
            return point.ClampToUnit();
        }

        //Integer dot product from two Points
        public static int DotProduct(Point a, Point b)
        {
            if (a == null || b == null)
                return 0;

            int dot = 0;
            dot += a.x * b.x;
            dot += a.y * b.y;
            dot += a.z * b.z;
            return dot;
        }
    }
}