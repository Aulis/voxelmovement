﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Custom UnityEvent classes for passing parameters dynamically in the editor.
namespace VoxelMovement
{
    [System.Serializable]
    public class UnityEventPoint : UnityEvent<Point> { }
}