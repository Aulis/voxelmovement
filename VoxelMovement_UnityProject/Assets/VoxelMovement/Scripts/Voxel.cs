﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The Voxel class represents a single "map square" in the VoxelMap.
//The VoxelMap generates the Voxels based on it's contents and volume
//A Voxel can hold a child object or be 'empty'
//A Voxel also holds references to any VoxelBodies currently occupying it's space
//It also holds references to it's neighbouring Voxels, and navigation properties. This is used for pathfinding.

namespace VoxelMovement
{
    public class Voxel : MonoBehaviour
    {

        public Point position;                                   //Voxel's position on the VoxelMap

        public GameObject child;                                 //The level element this Voxel is for. If null then Voxel is just air
        private List<VoxelBody> bodies = new List<VoxelBody>();  //List of VoxelBodies currently in this Voxel

        public VoxelNavigation.VoxelNavNode navNode = new VoxelNavigation.VoxelNavNode();   //Navigation properties for VoxelNavAgents

        //Neighbouring Voxels for pathfinding
        private Voxel neighbourForward;
        private Voxel neighbourRight;
        private Voxel neighbourBack;
        private Voxel neighbourLeft;
        private Voxel neighbourUp;
        private Voxel neighbourDown;

        //The Voxel holds the definition for the six cardinal directions
        public enum Directions
        {
            FORWARD,
            RIGHT,
            BACK,
            LEFT,
            UP,
            DOWN
        }

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public List<VoxelBody> Bodies { get { return bodies; } }

        public Voxel NeighbourForward { get { return neighbourForward; } }
        public Voxel NeighbourRight { get { return neighbourRight; } }
        public Voxel NeighbourBack { get { return neighbourBack; } }
        public Voxel NeighbourLeft { get { return neighbourLeft; } }
        public Voxel NeighbourUp { get { return neighbourUp; } }
        public Voxel NeighbourDown { get { return neighbourDown; } }

        //----------------------------------------------
        //VOXEL METHODS
        //----------------------------------------------

        //Method for getting a Point from a direction enum value
        public static Point GetPointDirection(Directions direction)
        {
            switch (direction)
            {
                case Directions.UP: return Point.up;
                case Directions.DOWN: return -Point.up;
                case Directions.LEFT: return -Point.right;
                case Directions.RIGHT: return Point.right;
                case Directions.FORWARD: return Point.forward;
                case Directions.BACK: return -Point.forward;
                default: return Point.zero;
            }
        }

        //Helper method for setting neighbours when map is being created
        public void SetNeighbours(Voxel forward, Voxel right, Voxel back, Voxel left, Voxel up, Voxel down)
        {
            neighbourForward = forward;
            neighbourRight = right;
            neighbourBack = back;
            neighbourLeft = left;
            neighbourUp = up;
            neighbourDown = down;
        }

        //When a VoxelBody moves into a Voxel, the Voxel gets a reference
        public void EnterVoxel(VoxelBody body)
        {
            if (bodies.Contains(body) == false)
                bodies.Add(body);
        }

        //When a VoxelBody leaves a Voxel, the reference is removed
        public void ExitVoxel(VoxelBody body)
        {
            bodies.Remove(body);
        }

        //Voxel is solid if it has an active child and it's layer is a match
        public bool IsSolid(LayerMask solidLayers)
        {
            return (child != null && child.activeInHierarchy == true && ((1 << child.layer & solidLayers.value) > 0)) ? true : false;
        }
    }
}