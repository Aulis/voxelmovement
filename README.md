# VoxelMovement #

VoxelMovement provides a map, movement and pathfinding scripts for games requiring grid based movement.

- - -

# About key scripts #

Here is a brief description of the key monobehaviour scripts and their editor properties. More documentation for non-monobehaviour scripts can be found in code.

## VoxelMap ##

The VoxelMap script holds the level data and properties for pathfinding.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelMap%20component.png)

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelMap%20table.png)

## VoxelGroup ##

The VoxelGroup script is used for grouping level elements. Can be used to define rooms and other areas.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelGroup%20component.png)

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelGroup%20table.png)

## VoxelNavProperties ##

The VoxelNavProperties script is used to set navigation related properties for voxels in the editor. Add this component to a level element.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelNavProperties%20component.png)

![Alth text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelNavProperties%20table.png)

## VoxelBody ##

The VoxelBody script handles the movement of gameobjects in the VoxelMap. Similar to Unity's RigidBody.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelBody%20component.png)

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelBody%20table.png)

## VoxelBodymanager ##

The VoxelManager script detects and solves collisions between VoxelBodies. Created automatically when VoxelBodies are in the scene and the scene is run.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelBodyManager%20component.png)

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelBodyManager%20table.png)

## VoxelNavAgent ##

The VoxelNavAgent script drives a VoxelBody along paths. Similar to Unity's NavAgent.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/VoxelNavAgent%20component.png)

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/ba021a3eac4dbd01bd4933545e9ce11042f37c9a/READMEImages/VoxelNavAgent%20table.png)

- - -

# Installation #

Project has been developed in Unity 2022.3.34. Functionality in newer versions is likely, but not quaranteed.

* Use the included Unity package file to import the scripts and examples to your project.
* In Unity choose 'Assets' -> 'Import package' -> 'Custom package'. Browse and select the VoxelMovement_2020-xx-xx.unitypackage file.
* The Examples folder includes an example level with a movable player, a pushable object and two moving AI players.

- - -

# Usage #

## Creating a level ##

* First decide on a unit size for your map then design your level elements to that size. Configure your snap settings or ProGrids package accordingly.
* Add VoxelMap to an empty gameobject and set the map's unit size.
* Add level elements as children to your map object. Use either Unity's snap settings or the ProGrids package to align them to a grid. Set solid elements to a layer of your choice.
* When the scene is run the map aligns the level elements to a grid and places them as children of voxel objects.
* If you wish to structure your level hierarchy you can use the VoxelGroup script.
* Create an empty gameobject for the group as a child under your map object and add the VoxelGroup script.
* Set the groupTransform reference and add your level elements under the group object.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/Map%20example1.png =100x20)

## Adding a moving object ##

* Add the VoxelBody script to the object you want to move.
* Configure the movement and rotation curves and solid layers. Include the layers of solid level elements and VoxelBodies.
* If you wish to move the object add a script that controls the VoxelBody.
* To move the object call the AddMovement and AddRotation methods.
* For additional control see the action delegates and UnityEvents in VoxelBody or the ExampleVoxelBodyInput or TestVoxelNavAgent in the Examples folder.

## Pathfinding ##

* Add the VoxelNavAgent component to a gameobject with the VoxelBody component.
* Set the VoxelBody reference the agent is using and configure the movement and rotation curves.
* Set your level's solid layers to the avoid layers.
* Configure other navigation values for your needs.
* If you enabled AutoUpdate, then calling SetDestination is all that is needed for the agent to begin moving.
* If you disabled AutoUpdate, then call UpdateVoxelBody when the agent is stopped to advance to the next voxel.
* For additional control see the action delegates in VoxelNavAgent or TestVoxelnavAgent in the Examples folder.

![Alt text](https://bitbucket.org/Aulis/voxelmovement/raw/48047000ff0d3c5311ddefdb3206a273bfcd2ad5/READMEImages/NavAgent%20example.png)

- - -

# License #

MIT License

Copyright (c) 2020 Petteri Saarela

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

